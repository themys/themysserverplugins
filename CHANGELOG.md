# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

Guiding Principles:

- Changelogs are for humans, not machines.
- There should be an entry for every single version.
- The same types of changes should be grouped.
- Versions and sections should be linkable.
- The latest version comes first.
- The release date of each version is displayed.
- Mention whether you follow Semantic Versioning.

Types of changes:

- **Added** for new features.
- **Changed** for changes in existing functionality.
- **Deprecated** for soon-to-be removed features.
- **Removed** for now removed features.
- **Fixed** for any bug fixes.
- **Security** in case of vulnerabilities.

## Unreleased

### Fixed

- Fix integer promotion check from unsigned to int

### Added

- Add CEASmoothInterfacesFilter.

## Version 1.1.2 - 2025-01-09 -- Paraview needed at commit c0b74f15 or newer

### Fixed

- Use new image comparison mechanism when testing. [See here](https://discourse.paraview.org/t/how-to-use-the-ssim-image-comparison-mechanism-when-testing-paraview-plugins-and-paraview-based-applications/15538) for more information

- Fix type for target rcv buffer since paraview change

- Fix icon for CEA Annotate Time

- Removes useless TJunction filter

- Fix clang-tidy warnings on FastDecimatedSurfaceRepresentation target

- Fix clang-tidy warnings on SelectionCellLabel target

- Fix clang-tidy warnings on HyperTreeGridFragmentation target

- Fix clang-tidy warnings on HDFFieldDataReader target

- Fix clang-tidy warnings on DataSelectionExtraction target

- Fix clang-tidy remarks on target ConfigurableArrayListDomain

- Fix clang-tidy warnings on CEACellDataToPointData target

- Fix clang-tidy warnings on LineSources target

- Fix clang-tidy warnings on ComparisonFilter target

### Added

### Changed

- Removes use of environment variable `VTK_TEMP_DIR` for test image comparison.

- Renames the property `FileAvailabilityCheckToken` into `FileAvailabilityCheckRegex` in
  accordance with what is done in the CEAHerculeReader

- Add python wrapping of the filters modules

- Add a second boundary condition for `CEACellDataToPointData` filter.

- Add the CEAAnnotateTime filter. Deprecate the AnnotateTimeCEA filter.

  Closes #67 #68 #69

- Add the possibility to run code coverage measurments through `ctest` test suite

### Changed

- Use new commontools CI files (simplify iwyu and cppcheck jobs)

- Remove install of `python3-matplotlib` in the CI steps (the package is present in the image).

- Use new CppCheck analysis tool from commontools. Fix cppcheck warnings.

- Treats clang-tidy warnings as errors and fixes those warnings/errors

- The XML test registration function now checks the existence of mandatory target ParaView::paraview

  Closes #70

- Moves IWYU CMake and CI machinery into themyscommontools repository

- Fixes wrong commontools branch name in .gitlab-ci.yml and CMakeLists.txt

## Version 1.1.1 - 2024-09-11 -- Paraview needed at commit 1eb2dd73 or newer

### Fixed

- Update image references due to changes in PV background and color scale default values

- Fix build for headless server
- Fix an issue with FastDecimateRepresentation freezing in C/S mode
  in parallel with temporal data when changing timesteps
- Fix an issue with FastDecimateRepresentation warning about missing
  color array in C/S mode in parallel

### Added

### Changed

- Remove CMake duplicated testing functions in Plugins/ThemysFilters and
Plugins/ThemysRepresentations and gather them to a dedicated module in
the Plugins/cmake folder

## Version 1.1.0 - 2024-09-06 -- Paraview needed at commit 1eb2dd73 or newer

### Fixed

- Fix link error due to new location of "MergeBlocks.h" file in recent versions of ParaView

### Added

- Add FileAvailability checking settings

- Add HDFFieldDataReader that reads field data for all timesteps in a vtkTable from a vtkHDF file.

### Changed

- Takes into account change in the way commontools handles `clang-tidy`.
  Now the `CXX_CLANG_TIDY` target property is used.

## Version 1.0.9 - 2024-06-28 -- Paraview needed at commit dd91a67a or newer

### Fixed

### Added

## Version 1.0.8 - 2024-06-20 -- Paraview needed at commit 73a99c7f or newer

### Fixed

- Adds a CleanPolyData operation at the end of the vtkMaterialInterface process

  Closes #64
  Closes #66

- Updates contour references following VTK work on contouring
- Fix an issue with FastDecimateRepresentation crashing in C/S mode
  (empty input not handled correctly)

### Added

- Adds properties to choose the meshes, materials and data that are selected by default

### Changed
- Fast Decimated Surface options are hidden if the representation is not selected

- vtkCellId, vtkNodeId, vtkDomainId are selected by default

- Angle parameters for the Rotate (X,Y,Z) filters are now in the default panel (instead of advanced)

- Removes CEAHyperTreeGridGenerateMaskLeavesCells filter and uses HyperTreeGridVisibleLeavesSize from VTK instead with updated variable names

- Fetching content of `commontools` repo is made for debug builds only

- Changes the repository of the image used for CI

## Version 1.0.7 - 2024-04-02 -- Paraview needed at commit 8f4e48bc or newer

### Fixed

- Fix style
- Fix an issue with FastDecimateRepresentation crashing in C/S mode
- Fix the GUI toolbars bugs
- Fix an issue with FastDecimateRepresentation causing random segfaults

### Added

### Changed

- Use common repo for storing and mutualizing CMake scripts

- Use common repo for storing and mutualizing CI across Themys repositories

- Uniformize the formatting tools across the whole project.
    - .pre-commit-config.yaml file is identical to the one used in readers project
    - .clang-format file is identical to the one used in readers project (except some specificities of readers project)

- Factorizes CI jobs

## Version 1.0.6 - 2023-12-06

### Fixed

- Use run-clang-tidy-15 in accordance with the tools available in the Docker
  image used for the CI

### Added

- Add test for CEAEducational Filter

### Changed

- Do not install tools for code quality checks in the CI because
  they are already present in the Docker image

- Replace advanced mode by interface mode setting

## Version 1.0.5 - 2023-10-02

### Fixed

- Compilation without Qt

### Added

## Version 1.0.4 - 2023-09-25

### Changed

- The legacy build job in the CI uses gcc-8.

- Revert the launch of MPI tests in the CI
  (random crash due to X server connection problem).

- Fixes compilation failure due to the use of new VTK version
  where the Implicit Arrays are not a module anymore but are
  part of the CommonCore module.

- The CI now runs the MPI tests. Use saas-linux-large-amd64 machine type which
  offers 8 CPUs and 32 GB of RAM.

- The fragmentation filter is now part of this project.
  It is renamed `HyperTreeGridFragmentation`.

  Closes #44

- Apart from a maintenance effort (appropriation, comments,
  optimization and better consideration of ghost cells in the
  parallel case), the fragmentation filter has been enhanced
  with the ExtractType option allowing you to choose the type
  of fragment extraction:

  - none,
  - than the centers of the side cells;
  - centers of all cells.

  It should be noted that the extraction by the centers of the side
  cells of the fragments requires to have applied before the call
  of this filter a GhostCellsGenetaor filter. Otherwise, sides
  will be identified at the borders following the distribution on the servers.

  It is now possible to optionally output the global fields on the
  centers of the extraction cells via EnableOuputGlobalFields option.
  Using implicit arrays, the memory overhead of this activation is almost
  nil... except when a Save Data is triggered.

  A FormFactorRadius is now available allowing to define a
  spherical form factor for each fragment using the average center
  (the average of the position of the centers of the cells that make
  up each fragment) or the barycenter (the centers weighted by the mass,
  the all divided by the global mass). The barycenter is avalaible
  only if define a MassName ou DensityName.
  The choice for the centers definition prefer barycenter on average center.

  Closes #52

- The tests and baseline directories have been reorganized with a subdirectory
  for each filter tested.

- The default value of the "ZEpsilon" property is changed to 0.0001.

  Closes #41

### Fixed

- Fix major `cppcheck` warnings

- Avoid holes in the final mesh (Emmental aspect) when using the  `vtkMaterialInterface` filter,
  if the current PV server have a normal array with all components that are zero and
  other arrays (order and distance) have default values (order = -1 and distance = 0)

  Closes #37

### Added

- Iwyu is used for the `vtkHyperTreeGridFragmentationFilter`.

- Bug fixes and improvements of the `vtkHyperTreeGridGeometry` filter have been
  transferred into `VTK` sources.
  Adds tests on the `vtkHyperTreeGridGeometry` filter to check this transfer is ok.

  Closes #42

- Each test is labeled with the name of the directory it lay in.

- Adds a `CMake` target named `RunClangTidy` that launches `clang-tidy` analysis
  on the whole project. Also adds a job in the CI that uses this target.

- Adds a `CMake` target named `RunCppCheck` that launches `cppcheck` analysis
  on the whole project. Also adds a job in the CI that uses this target.

- Adds the `CEACellDataToPointData` filter. The goal of this filter is to compute
  a projection on an unstructured mesh that is in fact a partially defined structured
  mesh made of quadrangles (2D) or hexaedrons (3D). Missing part of the mesh
  are considered filled with a null volumic fraction.

  ┌───┬───┬───┬───┬───┐
  │   │   │   │   │   │
  ├───┼───┼───┼───┼───┤
  │   │   │   │   │   │
  ├───┼───┼───┼───┼───┤
  │   │   │0.5│0.2│   │
  └───┴───┴───┼───┼───┼───┬───┐
            X │0.4│   │   │   │
              ├───┼───┼───┼───┤
              │   │   │   │   │
              ├───┼───┼───┼───┤
              │   │   │   │   │
              └───┴───┴───┴───┘

  For example, the quantity computed on the node X will be
  (0.5 + 0.2 + 0.4 + 0) / 4 and not (0.5 + 0.2 + 0.4) / 3 as it would be with
  the standard CellDataToPointData filter.

  Closes #38
  Closes #39

- Adds HyperTree Grid Fragmentation filter parallel capabilities

  Closes #51

- Fix HyperTree Grid Ghost Cells Generator filter

  A option allows you to visualize the mesh with the ghost cells.

- Adds HyperTree Grid Generate Mask Leaves Cells

  Adds vtkValidCell field on the cells set to 1 if the cell is valid
  (leafed, unmasked and unghosted) otherwise 0 (coarse, masked or ghosted)

  Adds vtkVolume field to the cells

  These new fields are essential to calculate global fields on HTG.
  Thus the true calculation of global mass with Python Calculator filter becomes :
    sum(Mass * vtkValidCell).
  Thus the true calculation of average density with Python Calculator filter becomes :
    sum(Density * vtkVolume * vtkValidCell) / sum(vtkVolum * vtkValidCell)

## Version 1.0.3 - 2023-05-03

### Added

- Introduces the `vtkContourWriter` module that extract contours and write them
  down into a `.dat` file. Each block has its contour extracted and it forms a line
  described as a collection of points in the `.dat` file. Each collection of points
  is separated by a `&` character followed by a new line.
  This module works in parallel (C/S) mode.

  Closes #30

- Adds an educational plugin (`CEAEducationalFilter`) for custom filter.

### Fixed

- Fixed the CXX standard in MaterialFilters and adds build test with `gcc10` to
  ensure the CXX standard is correctly taken into account.

- The Themys's specific property `PipelineName` is replaced with the ParaView's
  propery `RegistrationName`.

- Fixes compilation error when using `clang-tidy`.

  Closes #34

- Avoid holes in the final mesh (Emmental aspect) if the current PV server
  does not have the interface distance array and other arrays (normal and order)
  have default values (normal = [0., 0., 0.] and order = -1).

  Closes #31

- Fixes the bug that make Themys crash when applying the `vtkMaterialInterface` filter
  on a data object that holds points arrays.

  Closes #33

### Removed

- The old filter that was used to extract contour (only in sequential mode) is
  removed because the `vtkContourWriter` replaces it.

## Version 1.0.2 - 2023-02-20

### Added

- Introduces storage settings for uses in the assistant

- In case the FillMaterial option is on, the output meshes have the same type
  as the input meshes

  Closes #27

### Fixed

- Fixes the way the types of the cells produced by the clip method are attributed

  Closes #25
  Closes #26

- Fixes the wrong values of the cell fields after applying the vtkMaterialInterface filter.

  Closes #28

- Reduces the amount of log messages.

  Closes #24

- Adds property settings to set the regex used for renaming a source in the pipeline

- Avoid holes in the final mesh (Emmental aspect) if the current PV server
  does not have any interface arrays.

  Closes #22

- Avoid SIGSEGV crashes when interface data are ill formed. Also checks the
  validity of the order array.

  Closes #23

## Version 1.0.1 - 2022-12-19

### Fixed

- Fixes the writer to dat files. This writer allows contour extraction on

  - MultiBlockDataSet
  - PolyData
  - UnstructuredGrid

  and writes the obtained contour into a file with `.dat` extension.

  To use it, just click on `File` -> `Save Data...` then select the field
  `CEA Writer DAT (Python)(*.dat)` in the `Files of type:` selection box.

  Closes #17

## Version 1.0.0 - 2022-11-25

### Fixed

- Fixes the transfer of the Field data through the vtkMaterialInterface filter.
  Closes #14

- Fixes the missing interface for some materials problem.
  The topology construction of the last material of the mixed cell was failing.
  Closes #12

## Version 0.3.3 - 2022-07-18
