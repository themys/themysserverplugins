#include "vtkFastDecimatedSurfaceRepresentation.h"

#include <array>
#include <cassert>
#include <cmath>    // for pow
#include <iostream> // for std::cerr et std::endl
#include <limits>
#include <set>
#include <string> // for basic_string, string
#include <vector>

#include <vtkAlgorithm.h> // for vtkAlgorithm
#include <vtkCallbackCommand.h>
#include <vtkCellArray.h> // for vtkCellArray
#include <vtkCellArrayIterator.h>
#include <vtkCellData.h>
#include <vtkCellType.h>  // for VTK_QUAD
#include <vtkCommand.h>   // for vtkCommand
#include <vtkDataArray.h> // for vtkDataArray
#include <vtkDataObject.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkDoubleArray.h>
#include <vtkGenericDataArray.txx> // for vtkGenericDataArray::Allocate
#include <vtkIdList.h>             // for vtkIdList
#include <vtkIndent.h>             // for operator<<, vtkIndent
#include <vtkInformation.h>
#include <vtkMapper.h>
#include <vtkMath.h>
#include <vtkMergeBlocks.h>
#include <vtkNew.h> // for vtkNew
#include <vtkObjectFactory.h>
#include <vtkPVDataRepresentation.h> // for vtkPVDataRepresentation
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPolygon.h>
#include <vtkScalarsToColors.h>
#include <vtkSetGet.h>         // for vtkWarningMacro, vtkDebugMacro
#include <vtkSmartPointer.h>   // for vtkSmartPointer, TakeSmartPointer
#include <vtkSystemIncludes.h> // for vtkOStreamWrapper
#include <vtkTuple.h>          // for vtkTuple
#include <vtkType.h>           // for vtkIdType
#include <vtkVector.h>
#include <vtkView.h>

class vtkInformationVector;

vtkStandardNewMacro(vtkFastDecimatedSurfaceRepresentation);

//----------------------------------------------------------------------------
vtkFastDecimatedSurfaceRepresentation::vtkFastDecimatedSurfaceRepresentation()
{
  // Setup observer for logarithmic scale
  this->LogObserver->SetCallback(
      &vtkFastDecimatedSurfaceRepresentation::LogCallback);
  this->LogObserver->SetClientData(this);
}

//----------------------------------------------------------------------------
vtkFastDecimatedSurfaceRepresentation::~vtkFastDecimatedSurfaceRepresentation()
{
  if (this->LookupTable != nullptr)
  {
    this->LookupTable->RemoveAllObservers();
  }
}

//----------------------------------------------------------------------------
void vtkFastDecimatedSurfaceRepresentation::SetInputArrayToProcess(
    int idx, int port, int connection, int fieldAssociation, const char* name)
{
  this->Superclass::SetInputArrayToProcess(idx, port, connection,
                                           fieldAssociation, name);
  this->MarkModified();
}

//----------------------------------------------------------------------------
void vtkFastDecimatedSurfaceRepresentation::SetNumberOfColors(int numColors)
{
  if (this->NumberOfColors != numColors)
  {
    this->NumberOfColors = numColors;
    this->MarkModified();
  }
}

//----------------------------------------------------------------------------
void vtkFastDecimatedSurfaceRepresentation::SetMaxMergeAngle(double angle)
{
  constexpr auto FLAT_ANGLE = 180.0;
  if (this->MaxMergeAngle != angle)
  {
    // Values are clamped between 0 and 180 degrees
    if (angle < 0.0)
    {
      this->MaxMergeAngle = 0.0;
    } else if (angle > FLAT_ANGLE)
    {
      this->MaxMergeAngle = FLAT_ANGLE;
    } else
    {
      this->MaxMergeAngle = angle;
    }

    this->MarkModified();
  }
}

//----------------------------------------------------------------------------
void vtkFastDecimatedSurfaceRepresentation::SetLookupTable(
    vtkScalarsToColors* val)
{
  this->Superclass::SetLookupTable(val);

  // Update lookup table and set the observer
  if (this->LookupTable != val)
  {
    // Remove observer if there is one
    if (this->ObserverId != 0)
    {
      this->LookupTable->RemoveObserver(this->ObserverId);
    }

    this->LookupTable = val;
    this->ObserverId = this->LookupTable->AddObserver(vtkCommand::ModifiedEvent,
                                                      this->LogObserver);
  }
}

//----------------------------------------------------------------------------
void vtkFastDecimatedSurfaceRepresentation::UpdateLogScale()
{
  if (this->Mapper != nullptr && this->Mapper->GetLookupTable() != nullptr &&
      this->GetView() != nullptr)
  {
    const bool using_log_scale{
        this->Mapper->GetLookupTable()->UsingLogScale() != 0};
    if (this->LogScale != using_log_scale)
    {
      this->LogScale = using_log_scale;
      this->MarkModified();

      // This is necessary to ensure reexecution of the algorithm when
      // different changes to the color map are made
      this->GetView()->Update();
    }
  }
}

//----------------------------------------------------------------------------
// NOLINTNEXTLINE(readability-function-cognitive-complexity)
int vtkFastDecimatedSurfaceRepresentation::RequestData(
    vtkInformation* request, vtkInformationVector** inputVector,
    vtkInformationVector* outputVector)
{
  // Retrieve input
  vtkDataObject* inputObject = vtkDataObject::GetData(inputVector[0]);

  if (inputObject == nullptr)
  {
    // Input is optional and can be empty on the client or render server,
    // depending on the client-server mode (see vtkGeometryRepresentation)
    // We do not call RequestData of the Superclass since we want to force
    // the placeholder to be a vtkPolyData instance.
    const vtkNew<vtkPolyData> placeholder;
    this->ExecuteInternalPipeline(placeholder);
    // Update temporal information
    // Call of the grand-grand-parent (vtkPVDataRepresentation) method instead
    // of grand-parent one (vtkGeometryRepresentation) seems ok due to comment
    // in the previous line
    // NOLINTNEXTLINE(bugprone-parent-virtual-call)
    return vtkPVDataRepresentation::RequestData(request, inputVector,
                                                outputVector);
  }

  // Prepare internal input if needed
  vtkSmartPointer<vtkPolyData> internalInput;

  // Copy the input given that the representation is based on polygonal meshes
  if (inputObject->IsA("vtkPolyData") != 0)
  {
    internalInput = vtkPolyData::SafeDownCast(inputObject);
  }
  // Extract the surface if the input is an unstructured grid
  else if (inputObject->IsA("vtkUnstructuredGrid") != 0 ||
           inputObject->IsA("vtkStructuredGrid") != 0 ||
           inputObject->IsA("vtkImageData") != 0)
  {
    vtkNew<vtkDataSetSurfaceFilter> surfaceFilter;
    surfaceFilter->SetInputData(inputObject);
    surfaceFilter->Update();
    internalInput = surfaceFilter->GetOutput();
  }
  // Merge blocks to obtain a polygonal mesh
  else if (inputObject->IsA("vtkDataObjectTree") != 0)
  {
    // Merge blocks without merging points, so that two cells belonging to
    // different blocks are not considered as neighbors
    vtkNew<vtkMergeBlocks> mergeFilter;
    mergeFilter->SetInputData(inputObject);
    mergeFilter->SetMergePoints(false);

    // Extract the surface of the merge output
    vtkNew<vtkDataSetSurfaceFilter> surfaceFilter;
    surfaceFilter->SetInputConnection(mergeFilter->GetOutputPort());
    surfaceFilter->Update();
    internalInput = surfaceFilter->GetOutput();
  } else
  {
    vtkErrorMacro(<< "Unsupported input type.");
    return 1;
  }

  // Setup initial data set
  const vtkIdType numInCells = internalInput->GetNumberOfCells();
  vtkCellArray* inCells = internalInput->GetPolys();
  vtkPoints* inPoints = internalInput->GetPoints();
  const int fieldAssoc = this->GetInputArrayInformation(0)->Get(
      vtkDataObject::FIELD_ASSOCIATION());
  if (fieldAssoc != vtkDataObject::FIELD_ASSOCIATION_CELLS)
  {
    vtkWarningMacro(
        << "The vtkFastDecimatedSurfaceRepresentation needs active cell scalar "
           "array on input data to perform decimation.");
    this->ExecuteInternalPipeline(internalInput);
    // Update temporal information
    // Call of the grand-grand-parent (vtkPVDataRepresentation) method instead
    // of grand-parent one (vtkGeometryRepresentation) seems ok due to comment
    // in the line 142
    // NOLINTNEXTLINE(bugprone-parent-virtual-call)
    return vtkPVDataRepresentation::RequestData(request, inputVector,
                                                outputVector);
  }

  const std::string inScalarsName =
      this->GetInputArrayInformation(0)->Get(vtkDataObject::FIELD_NAME());
  vtkDataArray* inScalars =
      internalInput->GetCellData()->GetScalars(inScalarsName.c_str());

  // Display internal input if there is no cell data
  if (inScalars == nullptr)
  {
    // This can happen on some ranks with composite datasets, with some nodes
    // being converted into empty polydata during block merging.
    vtkDebugMacro(<< "Cannot retrieve " << inScalarsName << " on input data.");
    this->ExecuteInternalPipeline(internalInput);
    // Update temporal information
    // Call of the grand-grand-parent (vtkPVDataRepresentation) method instead
    // of grand-parent one (vtkGeometryRepresentation) seems ok due to comment
    // in the line 142
    // NOLINTNEXTLINE(bugprone-parent-virtual-call)
    return vtkPVDataRepresentation::RequestData(request, inputVector,
                                                outputVector);
  }

  // Create PolyData internal output for the representation
  vtkNew<vtkPolyData> internalOutput;
  vtkNew<vtkPoints> outPoints;
  vtkNew<vtkCellArray> outCells;
  vtkNew<vtkDoubleArray> outScalars;
  vtkIdType numOutPoints = 0;
  vtkIdType numOutCells = 0;

  outPoints->SetNumberOfPoints(inPoints->GetNumberOfPoints());
  outCells->AllocateEstimate(numInCells, inCells->GetMaxCellSize());
  outScalars->SetName(inScalars->GetName());
  outScalars->Allocate(numInCells);

  // Compute cosine of the maximum angle between cell planes and cell edges for
  // merging
  const double maxCosAngle =
      std::cos(vtkMath::RadiansFromDegrees(this->MaxMergeAngle));

  // Retrieve scalar data range
  double* scalarRange = inScalars->GetRange();

  // If there are non positive range values, do not use log scale
  if (this->LogScale && scalarRange[0] <= 0.0)
  {
    vtkWarningMacro(<< "The scalar range contains negative values. Changing "
                       "range to remove negative values.");

    // Change values to match color map editor when log scale is activated on
    // negative values
    constexpr auto MIN_SCALAR_RANGE = 0.0001;
    scalarRange[0] = MIN_SCALAR_RANGE;
    scalarRange[1] = scalarRange[1] <= 0.0 ? 1.0 : scalarRange[1];
  }

  if (this->LogScale)
  {
    scalarRange[0] = std::log10(scalarRange[0]);
    scalarRange[1] = std::log10(scalarRange[1]);
  }

  // The ith element of binValues is the center value of the ith bin.
  // These discrete values become the new cell scalar values for each cell of
  // the final dataset.
  std::vector<double> binValues(this->NumberOfColors);

  for (vtkIdType binId = 0; binId < this->NumberOfColors; ++binId)
  {
    assert(binId > std::pow(-10, std::numeric_limits<double>::digits10));
    assert(binId < std::pow(10, std::numeric_limits<double>::digits10));
    const auto binId_f = static_cast<double>(binId);
    // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    if (this->LogScale)
    {
      binValues[binId] = std::pow(
          10.0, scalarRange[0] + (2.0 * binId_f + 1.0) *
                                     (scalarRange[1] - scalarRange[0]) /
                                     (2.0 * this->NumberOfColors));
    } else
    {
      binValues[binId] =
          scalarRange[0] + (2.0 * binId_f + 1.0) *
                               (scalarRange[1] - scalarRange[0]) /
                               (2.0 * this->NumberOfColors);
    }
    // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
  }

  // Assign each cell to a bin based on the scalar data range
  std::vector<vtkIdType> cellToBin(numInCells);
  vtkIdType binId{0};

  for (vtkIdType cellId = 0; cellId < numInCells; ++cellId)
  {
    if (this->LogScale)
    {
      // If the cell value is negative or zero, the cell is assigned to the
      // first bin
      if (inScalars->GetTuple(cellId)[0] > 0.0)
      {
        binId = vtkMath::Floor(
            (std::log10(inScalars->GetTuple(cellId)[0]) - scalarRange[0]) /
            (scalarRange[1] - scalarRange[0]) * this->NumberOfColors);
      } else
      {
        binId = 0;
      }
    } else
    {
      binId = vtkMath::Floor((inScalars->GetTuple(cellId)[0] - scalarRange[0]) /
                             (scalarRange[1] - scalarRange[0]) *
                             this->NumberOfColors);
    }

    // Values are clamped to the scalar data range
    if (binId < 0)
    {
      cellToBin[cellId] = 0;
    } else if (binId >= this->NumberOfColors)
    {
      cellToBin[cellId] = this->NumberOfColors - 1;
    } else
    {
      cellToBin[cellId] = binId;
    }
  }

  // Compute input cells normals
  vtkNew<vtkDoubleArray> inNormals;
  vtkVector3d normal;
  const vtkIdType* cellPointIds = nullptr;
  vtkIdType numCellPts = 0;

  inNormals->SetNumberOfComponents(3);
  inNormals->Allocate(3 * numInCells);

  auto cellIter = vtk::TakeSmartPointer(inCells->NewIterator());
  for (cellIter->GoToFirstCell(); !cellIter->IsDoneWithTraversal();
       cellIter->GoToNextCell())
  {
    cellIter->GetCurrentCell(numCellPts, cellPointIds);
    assert(numCellPts > std::numeric_limits<int>::min());
    assert(numCellPts < std::numeric_limits<int>::max());
    vtkPolygon::ComputeNormal(inPoints, static_cast<int>(numCellPts),
                              cellPointIds, normal.GetData());
    inNormals->InsertTuple(cellIter->GetCurrentCellId(), normal.GetData());
  }

  // Prepare for cell traversal
  internalInput->BuildLinks();
  vtkNew<vtkIdList> neiCellIds;
  vtkNew<vtkIdList> neiPointIds;
  vtkNew<vtkIdList> nonQuadCellIds;
  vtkNew<vtkIdList> startingCellPointIds;
  vtkIdType numPoints = 0;
  vtkIdType cellId = 0;
  vtkIdType neiCellId = 0;
  vtkIdType newCellId = 0;
  vtkIdType startingCellId = 0;
  std::array<vtkIdType, 4> newCellPointIds{};
  std::array<vtkIdType, 4> startingCellIds{};
  std::array<std::array<vtkIdType, 2>, 4> startingEdgePointIds{};
  vtkVector3d cellNormal;
  vtkVector3d neiNormal;
  vtkVector3d neiFirstSideEdge;
  vtkVector3d neiSecondSideEdge;
  vtkVector3d outPtCoords;
  std::array<vtkVector3d, 4> cellEdges{};
  std::array<vtkVector3d, 4> cellPtCoords{};
  std::array<bool, 4> isDirectionValid{};
  std::set<vtkIdType> markedCells;
  std::set<vtkIdType> candidateCells;
  std::vector<vtkIdType> outPointsIds(inPoints->GetNumberOfPoints(), -1);

  // Merged cells are always quads
  nonQuadCellIds->Allocate(4);

  // Traverse cells for merging. During traversal, eligible cells are marked and
  // merged progressively. For each cell, the search for merge candidates is
  // done in a counterclockwise manner, alternating the search direction to
  // favor the creation of new cells as square as possible.
  cellIter = vtk::TakeSmartPointer(inCells->NewIterator());
  for (cellIter->GoToFirstCell(); !cellIter->IsDoneWithTraversal();
       cellIter->GoToNextCell())
  {
    cellIter->GetCurrentCell(numPoints, cellPointIds);
    cellId = cellIter->GetCurrentCellId();

    // Ignore cell if it has already been visited or merged
    if (markedCells.count(cellId) == 0)
    {
      if (internalInput->GetCellType(cellId) == VTK_QUAD)
      {
        // Mark cell as visited
        markedCells.insert(cellId);

        // Store information about the initial cell
        inNormals->GetTuple(cellId, cellNormal.GetData());

        for (vtkIdType idx = 0; idx < 4; ++idx)
        {
          inPoints->GetPoint(cellPointIds[idx], cellPtCoords.at(idx).GetData());
        }

        for (vtkIdType dir = 0; dir < 4; ++dir)
        {
          // Initialize edge vectors of the initial cell for angle checks
          for (int coord = 0; coord < 3; ++coord)
          {
            cellEdges.at(dir)[coord] = cellPtCoords.at((dir + 1) % 4)[coord] -
                                       cellPtCoords.at(dir)[coord];
          }

          cellEdges.at(dir).Normalize();

          // Initialize vertices for new cell definition
          newCellPointIds.at(dir) = cellPointIds[dir];

          // Initialize edge points and corresponding cell to start searching
          startingEdgePointIds.at(dir)[0] = cellPointIds[dir];
          startingEdgePointIds.at(dir)[1] = cellPointIds[(dir + 1) % 4];
          startingCellIds.at(dir) = cellId;

          // Reset search information
          isDirectionValid.at(dir) = true;
        }

        bool allDirectionsChecked = false;

        // Continue searching if there are still directions to explore
        while (!allDirectionsChecked)
        {
          for (vtkIdType dir = 0; dir < 4; ++dir)
          {
            // Skip if this direction has already been invalidated
            if (!isDirectionValid.at(dir))
            {
              continue;
            }

            // Start with the neighbor cell
            internalInput->GetCellEdgeNeighbors(
                startingCellIds.at(dir), startingEdgePointIds.at(dir)[0],
                startingEdgePointIds.at(dir)[1], neiCellIds);

            // A cell merge is only considered when the edge is shared by
            // exactly two cells
            if (neiCellIds->GetNumberOfIds() != 1)
            {
              isDirectionValid.at(dir) = false;
              continue;
            }

            neiCellId = neiCellIds->GetId(0);

            // Ignore neighbor if it has already been visited or merged, if it
            // is not a quad, or if it does not belong to the same scalar bin
            if (markedCells.count(neiCellId) != 0 ||
                internalInput->GetCellType(neiCellId) != VTK_QUAD ||
                cellToBin[cellId] != cellToBin[neiCellId])
            {
              isDirectionValid.at(dir) = false;
              continue;
            }

            // Define additional vectors used to verify angles between cells
            internalInput->GetCellPoints(neiCellId, neiPointIds);

            for (vtkIdType idx = 0; idx < 4; ++idx)
            {
              inPoints->GetPoint(neiPointIds->GetId(idx),
                                 cellPtCoords.at(idx).GetData());
            }

            for (int coord = 0; coord < 3; ++coord)
            {
              neiFirstSideEdge[coord] = cellPtCoords.at(dir)[coord] -
                                        cellPtCoords.at((dir + 3) % 4)[coord];
              neiSecondSideEdge[coord] = cellPtCoords.at((dir + 1) % 4)[coord] -
                                         cellPtCoords.at(dir)[coord];
            }

            neiFirstSideEdge.Normalize();
            neiSecondSideEdge.Normalize();
            inNormals->GetTuple(neiCellId, neiNormal.GetData());

            // Check whether the angles between cell planes and edges are small
            // enough
            if (cellNormal.Dot(neiNormal) < maxCosAngle ||
                cellEdges.at((dir + 3) % 4).Dot(neiFirstSideEdge) <
                    maxCosAngle ||
                cellEdges.at(dir).Dot(neiSecondSideEdge) < maxCosAngle)
            {
              isDirectionValid.at(dir) = false;
              continue;
            }

            // Mark the cell as valid candidate
            candidateCells.insert(neiCellId);
            startingCellId = neiCellId;

            // Now that the neighbor has been validated, we need to extend the
            // search along the side of the original cell to avoid having a
            // non-convex cell
            while (neiPointIds->GetId((dir + 2) % 4) !=
                   newCellPointIds.at((dir + 1) % 4))
            {
              // Start with the neighbor cell
              internalInput->GetCellEdgeNeighbors(
                  neiCellId, neiPointIds->GetId((dir + 1) % 4),
                  neiPointIds->GetId((dir + 2) % 4), neiCellIds);

              // Stop if the edge is not shared by exactly two cells
              if (neiCellIds->GetNumberOfIds() != 1)
              {
                isDirectionValid.at(dir) = false;
                candidateCells.clear();
                break;
              }

              neiCellId = neiCellIds->GetId(0);

              // Stop if the neighbor has already been visited or merged, if it
              // is not a quad, or if it does not belong to the same scalar bin
              if (markedCells.count(neiCellId) != 0 ||
                  candidateCells.count(neiCellId) != 0 ||
                  internalInput->GetCellType(neiCellId) != VTK_QUAD ||
                  cellToBin[cellId] != cellToBin[neiCellId])
              {
                isDirectionValid.at(dir) = false;
                candidateCells.clear();
                break;
              }

              // Define additional vectors to verify angles between cells
              internalInput->GetCellPoints(neiCellId, neiPointIds);

              for (vtkIdType idx = 0; idx < 4; ++idx)
              {
                inPoints->GetPoint(neiPointIds->GetId(idx),
                                   cellPtCoords.at(idx).GetData());
              }

              for (int coord = 0; coord < 3; ++coord)
              {
                neiFirstSideEdge[coord] =
                    cellPtCoords.at((dir + 1) % 4)[coord] -
                    cellPtCoords.at(dir)[coord];
              }

              neiFirstSideEdge.Normalize();
              inNormals->GetTuple(neiCellId, neiNormal.GetData());

              // Stop if the angles between cell planes and edges are too large
              if (cellNormal.Dot(neiNormal) < maxCosAngle ||
                  cellEdges.at(dir).Dot(neiFirstSideEdge) < maxCosAngle)
              {
                isDirectionValid.at(dir) = false;
                candidateCells.clear();
                break;
              }

              candidateCells.insert(neiCellId);
            }

            // Check that we are still good to go
            if (isDirectionValid.at(dir))
            {
              for (int coord = 0; coord < 3; ++coord)
              {
                neiSecondSideEdge[coord] =
                    cellPtCoords.at((dir + 2) % 4)[coord] -
                    cellPtCoords.at((dir + 1) % 4)[coord];
              }

              neiSecondSideEdge.Normalize();

              // Final angle check
              if (cellEdges.at((dir + 1) % 4).Dot(neiSecondSideEdge) >=
                  maxCosAngle)
              {
                // Update new cell
                inCells->GetCellAtId(startingCellId, startingCellPointIds);
                newCellPointIds.at(dir) = startingCellPointIds->GetId(dir);
                newCellPointIds.at((dir + 1) % 4) =
                    neiPointIds->GetId((dir + 1) % 4);

                // Mark all merged cells as treated
                markedCells.insert(candidateCells.begin(),
                                   candidateCells.end());

                // Update information to start searching for the next iteration
                startingCellIds.at(dir) = startingCellId;
                startingEdgePointIds.at(dir)[0] =
                    startingCellPointIds->GetId(dir);
                startingEdgePointIds.at(dir)[1] =
                    startingCellPointIds->GetId((dir + 1) % 4);

                startingCellIds.at((dir + 1) % 4) = neiCellId;
                startingEdgePointIds.at((dir + 1) % 4)[0] =
                    neiPointIds->GetId((dir + 1) % 4);
                startingEdgePointIds.at((dir + 1) % 4)[1] =
                    neiPointIds->GetId((dir + 2) % 4);
              } else
              {
                isDirectionValid.at(dir) = false;
              }

              candidateCells.clear();
            }
          }

          // Stop if there are no more valid directions to explore
          allDirectionsChecked = !(isDirectionValid[0] || isDirectionValid[1] ||
                                   isDirectionValid[2] || isDirectionValid[3]);
        }

        // Add points to the internal output if needed
        for (vtkIdType newPtId = 0; newPtId < 4; ++newPtId)
        {
          if (outPointsIds[newCellPointIds.at(newPtId)] == -1)
          {
            outPointsIds[newCellPointIds.at(newPtId)] = numOutPoints;
            inPoints->GetPoint(newCellPointIds.at(newPtId),
                               outPtCoords.GetData());
            outPoints->SetPoint(numOutPoints, outPtCoords.GetData());
            numOutPoints++;
          }
        }

        // Update new point IDs and add new cell
        for (vtkIdType idx = 0; idx < 4; ++idx)
        {
          newCellPointIds.at(idx) = outPointsIds[newCellPointIds.at(idx)];
        }

        newCellId = outCells->InsertNextCell(numPoints, newCellPointIds.data());
      } else
      {
        // Non-quad cells are simply copied
        for (vtkIdType idx = 0; idx < numPoints; ++idx)
        {
          // Add new point
          if (outPointsIds[cellPointIds[idx]] == -1)
          {
            outPointsIds[cellPointIds[idx]] = numOutPoints;
            inPoints->GetPoint(cellPointIds[idx], outPtCoords.GetData());
            outPoints->SetPoint(numOutPoints, outPtCoords.GetData());
            numOutPoints++;
          }
        }

        // Update new point IDs and add new cell
        nonQuadCellIds->SetNumberOfIds(numPoints);
        for (vtkIdType idx = 0; idx < numPoints; ++idx)
        {
          nonQuadCellIds->SetId(idx, outPointsIds[cellPointIds[idx]]);
        }

        newCellId = outCells->InsertNextCell(nonQuadCellIds);

        // Mark cell as visited
        markedCells.insert(cellId);
      }

      // Assign corresponding bin scalar value to the new cell
      outScalars->InsertNextValue(binValues[cellToBin[cellId]]);
      numOutCells++;
    }
  }

  // Define internal output
  outPoints->SetNumberOfPoints(numOutPoints);
  internalOutput->SetPoints(outPoints);
  internalOutput->SetPolys(outCells);
  internalOutput->GetCellData()->SetScalars(outScalars);
  internalOutput->Squeeze();

  this->ExecuteInternalPipeline(internalOutput);
  // Update temporal information
  // Call of the grand-grand-parent (vtkPVDataRepresentation) method instead
  // of grand-parent one (vtkGeometryRepresentation) seems ok due to comment
  // in the line 142
  // NOLINTNEXTLINE(bugprone-parent-virtual-call)
  return vtkPVDataRepresentation::RequestData(request, inputVector,
                                              outputVector);
}

//----------------------------------------------------------------------------
void vtkFastDecimatedSurfaceRepresentation::ExecuteInternalPipeline(
    vtkPolyData* dataToRender)
{
  this->GeometryFilter->SetInputDataObject(0, dataToRender);
  this->GeometryFilter->Modified();
  this->MultiBlockMaker->Update();
}

//----------------------------------------------------------------------------
void vtkFastDecimatedSurfaceRepresentation::PrintSelf(ostream& o_stream,
                                                      vtkIndent indent)
{
  this->Superclass::PrintSelf(o_stream, indent);
  o_stream << indent << "NumberOfColors: " << this->NumberOfColors << endl;
  o_stream << indent << "MaxMergeAngle: " << this->MaxMergeAngle << endl;
  o_stream << indent << "LogScale: " << this->LogScale << endl;
}
