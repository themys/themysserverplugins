# ~~~
# This function will wrap the vtk modules in arguments so that they can be used as Python classes (for use in filter for
# example).
#
# The python package will be built into "${CMAKE_BINARY_DIR}/${python_destination}/${CEA_PYTHON_PACKAGE}" directory. It will
# be installed in "${CMAKE_INSTALL_PREFIX}/${python_destination}/${CEA_PYTHON_PACKAGE}" directory.
#
# The default "python_destination" directory is "${CMAKE_INSTALL_LIBDIR}/python<VERSION>/site-packages"
#
# ARGUMENTS:
# * MODULES : list of vtk modules names that should be wrapped
# ~~~
function(themys_module_wrap_python)
  set(options)
  set(oneValueArgs LIBRARY_DESTINATION)
  set(multiValueArgs MODULES)
  cmake_parse_arguments(THEMYS_MODULE_WRAP_PYTHON "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  if(DEFINED THEMYS_MODULE_WRAP_PYTHON_KEYWORDS_MISSING_VALUES)
    message(
      FATAL_ERROR
        "In the call to themys_module_wrap_python function, the keywords ${THEMYS_MODULE_WRAP_PYTHON_KEYWORDS_MISSING_VALUES} are awaiting for at least one value"
    )
  endif()
  if(DEFINED THEMYS_MODULE_WRAP_PYTHON_UNPARSED_ARGUMENTS)
    message(
      FATAL_ERROR
        "Following arguments are unknown to themys_module_wrap_python function: ${THEMYS_MODULE_WRAP_PYTHON_UNPARSED_ARGUMENTS}"
    )
  endif()
  if(NOT DEFINED THEMYS_MODULE_WRAP_PYTHON_MODULES)
    message(FATAL_ERROR "The function themys_module_wrap_python is awaiting for MODULES keyword")
  endif()
  if(NOT DEFINED THEMYS_MODULE_WRAP_PYTHON_LIBRARY_DESTINATION)
    message(FATAL_ERROR "The function themys_module_wrap_python is awaiting for LIBRARY_DESTINATION keyword")
  endif()
  if(NOT DEFINED CEA_PYTHON_PACKAGE)
    message(
      FATAL_ERROR "The function themys_module_wrap_python requires the global variable CEA_PYTHON_PACKAGE to be defined"
    )
  endif()
  message(STATUS "Wrapping modules ${THEMYS_MODULE_WRAP_PYTHON_MODULES} into python classes")

  vtk_module_python_default_destination(python_destination)

  vtk_module_wrap_python(
    MODULES
    ${THEMYS_MODULE_WRAP_PYTHON_MODULES}
    MODULE_DESTINATION
    "${python_destination}"
    CMAKE_DESTINATION
    "${CMAKE_INSTALL_LIBDIR}/cmake/WrappingPython"
    LIBRARY_DESTINATION
    ${THEMYS_MODULE_WRAP_PYTHON_LIBRARY_DESTINATION}
    PYTHON_PACKAGE
    ${CEA_PYTHON_PACKAGE}
  )

  # Store the module name in a list that will be used to generate the __init__.py file of the CEA_PYTHON_PACKAGE
  foreach(module ${THEMYS_MODULE_WRAP_PYTHON_MODULES})
    if(NOT module IN_LIST THEMYS_PYTHON_MODULES)
      set(THEMYS_PYTHON_MODULES
          "${THEMYS_PYTHON_MODULES};${module}"
          CACHE INTERNAL "List of vtk modules that are wrapped into python classes"
      )
    endif()
  endforeach()

  # Deactivate IWYU analysis on the generated python targets
  foreach(module ${THEMYS_MODULE_WRAP_PYTHON_MODULES})
    set_target_properties(${module}Python PROPERTIES CXX_CPPCHECK "" CXX_CLANG_TIDY "" CXX_INCLUDE_WHAT_YOU_USE "")
  endforeach()

endfunction()

function(create_python_init_file)
  message(STATUS "Creating python init file for package ${CEA_PYTHON_PACKAGE}")
  # Create an `__init__.py` containing wrapped filters.
  set(python_modules)
  foreach(module ${THEMYS_PYTHON_MODULES})
    _vtk_module_get_module_property("${module}" PROPERTY "library_name" VARIABLE library_name)
    list(APPEND python_modules "'${library_name}'")
  endforeach()

  vtk_module_python_default_destination(python_destination)

  list(JOIN python_modules , python_modules_string)
  set(InitContent "__all__ = [${python_modules_string}]\n")
  file(
    GENERATE
    OUTPUT "${CMAKE_BINARY_DIR}/${python_destination}/${CEA_PYTHON_PACKAGE}/__init__.py"
    CONTENT "${InitContent}"
  )
  install(FILES "${CMAKE_BINARY_DIR}/${python_destination}/${CEA_PYTHON_PACKAGE}/__init__.py"
          DESTINATION "${python_destination}/${CEA_PYTHON_PACKAGE}/"
  )
endfunction()
