# This function registers a test, using paraview, which name is given in argument. The name is the name of the .xml file
# describing the test
function(_register_paraview_test plugin_name test_name with_baseline)
  # XML test need the GUI built by ParaView::paraview target
  if(NOT TARGET ParaView::paraview)
    return()
  endif()
  message(STATUS "Registering ParaView test ${test_name}")
  set("${test_name}_USES_DIRECT_DATA" ON)
  set("${test_name}_THRESHOLD" 0)

  set(baseline_option_and_dir)
  if(with_baseline)
    set(baseline_option_and_dir BASELINE_DIR "${CMAKE_SOURCE_DIR}/Data/Baseline")
  endif()

  paraview_add_client_tests(
    LOAD_PLUGIN
    "${plugin_name}"
    PLUGIN_PATH
    "$<TARGET_FILE_DIR:${plugin_name}>"
    ${baseline_option_and_dir}
    TEST_SCRIPTS
    "${test_name}.xml"
  )

  if(${CMAKE_VERSION} VERSION_LESS "3.22.0")
    get_property(
      "test_env_properties"
      TEST "pv.${test_name}"
      PROPERTY ENVIRONMENT
    )
    set_tests_properties(
      "pv.${test_name}"
      PROPERTIES ENVIRONMENT
                 "${test_env_properties};GALLIUM_DRIVER=llvmpipe;PARAVIEW_DATA_ROOT=${CMAKE_SOURCE_DIR}/Data"
                 FIXTURES_REQUIRED COVERAGE_FIXTURE
    )
  else()
    set_tests_properties(
      "pv.${test_name}"
      PROPERTIES ENVIRONMENT_MODIFICATION "GALLIUM_DRIVER=set:llvmpipe;PARAVIEW_DATA_ROOT=set:${CMAKE_SOURCE_DIR}/Data"
                 FIXTURES_REQUIRED COVERAGE_FIXTURE
    )
  endif()
endfunction()

# Convenience function for adding client tests with baseline
function(register_paraview_test_with_baseline plugin_name test_name)
  _register_paraview_test(${plugin_name} ${test_name} ON)
endfunction()

# Convenience function for adding client tests without baseline (or with inline comparisons)
function(register_paraview_test_without_baseline plugin_name test_name)
  _register_paraview_test(${plugin_name} ${test_name} OFF)
endfunction()

# This function registers a test running in client/server mode and using ParaView gui, which name is given in argument.
# The name is the name of the .xml file describing the test. The number of servers is given by the num_procs argument.
function(_register_paraview_client_server_test plugin_name test_name num_procs with_baseline)
  # XML test need the GUI built by ParaView::paraview target
  if(NOT TARGET ParaView::paraview)
    return()
  endif()
  message(STATUS "Registering client/server test ${test_name}")
  set("${test_name}_USES_DIRECT_DATA" ON)
  set("${test_name}_THRESHOLD" 0)

  set(baseline_option_and_dir)
  if(with_baseline)
    set(baseline_option_and_dir BASELINE_DIR "${CMAKE_SOURCE_DIR}/Data/Baseline")
  endif()

  paraview_add_client_server_tests(
    LOAD_PLUGIN
    "${plugin_name}"
    PLUGIN_PATH
    "$<TARGET_FILE_DIR:${plugin_name}>"
    ${baseline_option_and_dir}
    TEST_SCRIPTS
    "${test_name}.xml"
    NUMSERVERS
    "${num_procs}"
  )

  if(${CMAKE_VERSION} VERSION_LESS "3.22.0")
    get_property(
      "test_env_properties"
      TEST "pvcs.${test_name}"
      PROPERTY ENVIRONMENT
    )
    set_tests_properties(
      "pvcs.${test_name}"
      PROPERTIES
        ENVIRONMENT
        "${test_env_properties};GALLIUM_DRIVER=llvmpipe;PARAVIEW_DATA_ROOT=${CMAKE_SOURCE_DIR}/Data;SMTESTDRIVER_MPI_PREFLAGS=--use-hwthread-cpus"
        FIXTURES_REQUIRED
        COVERAGE_FIXTURE
    )
  else()
    set_tests_properties(
      "pvcs.${test_name}"
      PROPERTIES
        ENVIRONMENT_MODIFICATION
        "GALLIUM_DRIVER=set:llvmpipe;PARAVIEW_DATA_ROOT=set:${CMAKE_SOURCE_DIR}/Data;SMTESTDRIVER_MPI_PREFLAGS=set:--use-hwthread-cpus"
        FIXTURES_REQUIRED
        COVERAGE_FIXTURE
    )
  endif()
endfunction()

# Convenience function for adding client/server tests with baseline
function(register_paraview_client_server_test_with_baseline plugin_name test_name num_procs)
  _register_paraview_client_server_test(${plugin_name} ${test_name} ${num_procs} ON)
endfunction()

# Convenience function for adding client/server tests without baseline (or with inline comparisons)
function(register_paraview_client_server_test_without_baseline plugin_name test_name num_procs)
  _register_paraview_client_server_test(${plugin_name} ${test_name} ${num_procs} OFF)
endfunction()

# This function registers a test, using pvpython, which name is given in argument. The name is the name of the .py file
# describing the test
#
# Usually the python test is a configuration file that is configured through the configure_file command to generate a
# true python file in the test directory . Doing this ensures that if the python test file changes in the source tree,
# it will be also changed in the test directory.
#
# Here, this is not the case. Some code is factorized into the common module. As the code in it depends on the file
# system organization, it needs to be configured by CMake through the configure_file command. If the common.py.in file
# changes in the source tree then the common.py generated file in the test directory is will be updated consequently.
#
# However it will not be the case for the python test file. That's why we ensure it is copied into the test directory by
# using a CMake test fixture each time the test has to be run
function(_register_paraview_python_test plugin_name test_name)
  message(STATUS "Registering ParaView Python test ${test_name}")
  # Adds a test fixture (which has to be a CMake test) to copy the python test file in the vtk build test directory
  add_test("Copy-${test_name}" ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/${test_name}.py"
           ${_vtk_build_TEST_FILE_DIRECTORY}
  )
  # Declares this test to be a fixture test
  set_tests_properties("Copy-${test_name}" PROPERTIES FIXTURES_SETUP "${test_name}_fixture")
  # Adds the paraview python test
  paraview_add_test_python(NO_RT DIRECT_DATA "${test_name}.py")
  # Register some env variables for the test and declare that this test requires the fixture that copy the python test
  # file
  set_tests_properties(
    "Python-${test_name}"
    PROPERTIES
      ENVIRONMENT
      "GALLIUM_DRIVER=llvmpipe;PARAVIEW_DATA_ROOT=${CMAKE_SOURCE_DIR}/Plugin;PV_PLUGIN_PATH=$<TARGET_FILE_DIR:${plugin_name}>\;${CMAKE_BINARY_DIR}/ThemysFilters/PluginsPython"
      FIXTURES_REQUIRED
      "${test_name}_fixture;COVERAGE_FIXTURE"
  )
endfunction()

# This macro calls the _register_paraview_python_test and adds a label to the test. The label is the name of the
# directory the test lies in
macro(register_paraview_python_test plugin_name test_name)
  _register_paraview_python_test(${plugin_name} ${test_name})
  get_filename_component(TestDir ${CMAKE_CURRENT_SOURCE_DIR} NAME)
  set_tests_properties("Python-${test_name}" PROPERTIES LABELS "${TestDir}")
endmacro()

# This function is the same as register_paraview_python_test but it is used for // batch tests.
function(_register_paraview_pvbatch_mpi_test plugin_name test_name vtk_build_test nb_servers)
  message(STATUS "Registering ParaView pvbatch-mpi test ${test_name}")
  # Adds a test fixture (which has to be a CMake test) to copy the python test file in the vtk build test directory
  add_test("Copy-${test_name}" ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/${test_name}.py"
           ${_vtk_build_TEST_FILE_DIRECTORY}
  )
  # Declares this test to be a fixture test
  set_tests_properties("Copy-${test_name}" PROPERTIES FIXTURES_SETUP "${test_name}_fixture")
  # Dark Hack to avoid a crash during CMake configure step. Apparently, the _vtk_build_test variable is not set when
  # this function is called. So we set it manually here. It has to point to an existing target. But it is quite weird to
  # have to do this. TODO: open an issue on ParaView
  set(_vtk_build_test ${vtk_build_test})
  set(VTK_MPI_NUMPROCS ${nb_servers})
  # Adds the paraview pvbatch_mpi test
  paraview_add_test_pvbatch_mpi(NO_RT DIRECT_DATA "${test_name}.py")
  # Register some env variables for the test and declare that this test requires the fixture that copy the python test
  # file
  set_tests_properties(
    "${_vtk_build_test}Python-MPI-Batch-${test_name}"
    PROPERTIES
      ENVIRONMENT
      "GALLIUM_DRIVER=llvmpipe;PARAVIEW_DATA_ROOT=${CMAKE_SOURCE_DIR}/Plugin;PV_PLUGIN_PATH=$<TARGET_FILE_DIR:${plugin_name}>\;"
      FIXTURES_REQUIRED
      "${test_name}_fixture;COVERAGE_FIXTURE"
  )
endfunction()

# This macro calls the _register_paraview_python_test and adds a label to the test. The label is the name of the
# directory the test lies in. This macro accepts the name of the test as argument and the numbers of pvbatch instance to
# use
macro(register_paraview_pvbatch_mpi_test plugin_name test_name nb_servers)
  get_filename_component(TestDir ${CMAKE_CURRENT_SOURCE_DIR} NAME)
  # Here we assume that the directory name is also the name of an existing target
  set(_vtk_build_test ${TestDir})
  _register_paraview_pvbatch_mpi_test(${plugin_name} ${test_name} ${_vtk_build_test} ${nb_servers})
  set_tests_properties("${_vtk_build_test}Python-MPI-Batch-${test_name}" PROPERTIES LABELS "${TestDir};require-mpi")
endmacro()
