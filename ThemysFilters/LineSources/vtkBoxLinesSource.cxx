#include "vtkBoxLinesSource.h"

#include <array>
#include <cassert>
#include <cmath> // for pow
#include <limits>

#include <vtkCellArray.h>
#include <vtkLineSource.h>
#include <vtkNew.h>
#include <vtkObjectFactory.h>
#include <vtkPointSet.h> // for vtkPointSet
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkTuple.h> // for operator==, vtkTuple
#include <vtkVector.h>
// Need following include to have + operator defined
#include <vtkVectorOperators.h> // IWYU pragma: keep

vtkStandardNewMacro(vtkBoxLinesSource);

//----------------------------------------------------------------------------
vtkBoxLinesSource::vtkBoxLinesSource() { this->SetNumberOfInputPorts(0); }

//----------------------------------------------------------------------------
int vtkBoxLinesSource::RequestData(vtkInformation* /*request*/,
                                   vtkInformationVector** /*inputVector*/,
                                   vtkInformationVector* outputVector)
{
  constexpr const double HALF{0.5};
  vtkPolyData* output = vtkPolyData::GetData(outputVector, 0);

  if (output == nullptr)
  {
    vtkErrorMacro("Missing output!");
    return 1;
  }

  // Retrieve plane parameters
  const vtkVector3d center(this->PlaneCenter[0], this->PlaneCenter[1],
                           this->PlaneCenter[2]);
  const vtkVector3d normal(this->PlaneNormal[0], this->PlaneNormal[1],
                           this->PlaneNormal[2]);

  // Compute two orthogonal vectors in the plane using the x or y vector as
  // starting vectors
  vtkVector3d refVec(1.0, 0.0, 0.0);

  if (normal == refVec)
  {
    refVec[0] = 0.0;
    refVec[1] = 1.0;
  }

  vtkVector3d planeVec1(normal.Cross(refVec));
  planeVec1.Normalize();
  vtkVector3d planeVec2(normal.Cross(planeVec1));
  planeVec2.Normalize();

  // Compute a corner of the square
  const vtkVector3d corner =
      center - (planeVec1 + planeVec2) * (this->SideLength * HALF);
  const vtkIdType nbLines =
      this->NumberOfLinesPerSide * this->NumberOfLinesPerSide;

  // Create points
  vtkNew<vtkPoints> points;
  points->SetNumberOfPoints(nbLines * 2);
  std::array<double, 3> coords{0.0, 0.0, 0.0};
  vtkIdType currentNbPts = 0;

  // Create lines
  vtkNew<vtkCellArray> lines;
  lines->AllocateEstimate(nbLines, 2);

  // Create line generator
  vtkNew<vtkLineSource> lineSource;

  // If there is only on line, create it at the center
  if (this->NumberOfLinesPerSide == 1)
  {
    // Compute line extremities
    vtkVector3d point1 = center - (this->LinesLength * HALF) * normal;
    vtkVector3d point2 = center + (this->LinesLength * HALF) * normal;

    // Generate line
    lineSource->SetPoint1(point1.GetData());
    lineSource->SetPoint2(point2.GetData());
    lineSource->Update();

    // Retrieve line
    vtkPolyData* line = lineSource->GetOutput();
    vtkPoints* linePoints = line->GetPoints();
    vtkCellArray* lineCells = line->GetLines();

    // Add line points and segments to the output
    for (vtkIdType pt = 0; pt < 2; pt++)
    {
      linePoints->GetPoint(pt, coords.data());
      points->SetPoint(pt, coords.data());
    }

    lines->Append(lineCells, 0);
  } else
  {
    // digits10 returns the number of digits (in base-10) that can be
    // represented without loss
    assert(this->NumberOfLinesPerSide <
           std::pow(10, std::numeric_limits<double>::digits10));

    const double spacing =
        this->SideLength /
        (static_cast<double>(this->NumberOfLinesPerSide) - 1);

    for (vtkIdType m_index = 0; m_index < this->NumberOfLinesPerSide; m_index++)
    {
      for (vtkIdType n_index = 0; n_index < this->NumberOfLinesPerSide;
           n_index++)
      {
        // Compute line extremities and center
        const vtkVector3d pointOnPlane =
            corner + spacing * (m_index * planeVec1 + n_index * planeVec2);
        vtkVector3d point1 = pointOnPlane - (this->LinesLength * HALF) * normal;
        vtkVector3d point2 = pointOnPlane + (this->LinesLength * HALF) * normal;

        // Generate line
        lineSource->SetPoint1(point1.GetData());
        lineSource->SetPoint2(point2.GetData());
        lineSource->Update();

        // Retrieve line
        vtkPolyData* line = lineSource->GetOutput();
        vtkPoints* linePoints = line->GetPoints();
        vtkCellArray* lineCells = line->GetLines();

        // Add line points and segments to the output
        for (vtkIdType pt = 0; pt < 2; pt++)
        {
          linePoints->GetPoint(pt, coords.data());
          points->SetPoint(currentNbPts + pt, coords.data());
        }

        lines->Append(lineCells, currentNbPts);
        currentNbPts += 2;
      }
    }
  }

  // Fill output
  output->SetPoints(points);
  output->SetLines(lines);

  // Rotate lines if needed
  if (this->Angle != 0.0)
  {
    const vtkVector3d minusCenter = -1.0 * center;

    vtkNew<vtkTransform> transform;
    transform->PostMultiply();
    transform->Translate(minusCenter.GetData());
    transform->RotateWXYZ(this->Angle, normal.GetData());
    transform->Translate(center.GetData());

    vtkNew<vtkTransformFilter> transformer;
    transformer->SetTransform(transform);
    transformer->SetInputData(output);
    transformer->Update();
    output->ShallowCopy(transformer->GetOutput());
  }

  return 1;
}
