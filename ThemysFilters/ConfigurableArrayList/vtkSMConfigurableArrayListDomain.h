/**
 * @class   vtkSMConfigurableArrayListDomain
 * @brief   Use input array list and settings to set up default values
 *
 */

#ifndef vtkSMConfigurableArrayListDomain_h
#define vtkSMConfigurableArrayListDomain_h

#include <iosfwd> // for ostream

#include <vtkIOStream.h> // for ostream
#include <vtkSMArrayListDomain.h>
#include <vtkSetGet.h> // for vtkTypeMacro
class vtkIndent;

class vtkSMProperty;

class vtkSMConfigurableArrayListDomain : public vtkSMArrayListDomain
{
public:
  static vtkSMConfigurableArrayListDomain* New();
  vtkTypeMacro(vtkSMConfigurableArrayListDomain, vtkSMArrayListDomain);
  void PrintSelf(ostream& o_stream, vtkIndent indent) override;

  int SetDefaultValues(vtkSMProperty*, bool use_unchecked_values) override;

protected:
  vtkSMConfigurableArrayListDomain() = default;
  ~vtkSMConfigurableArrayListDomain() = default;

private:
  vtkSMConfigurableArrayListDomain(const vtkSMConfigurableArrayListDomain&) =
      delete;
  void operator=(const vtkSMConfigurableArrayListDomain&) = delete;
};

#endif
