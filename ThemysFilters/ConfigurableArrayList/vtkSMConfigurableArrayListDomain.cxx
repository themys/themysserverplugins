#include "vtkSMConfigurableArrayListDomain.h"

#include <sstream>
#include <string>
#include <vector>

#include <vtkIndent.h> // for vtkIndent
#include <vtkObjectFactory.h>
#include <vtkSMProperty.h>
#include <vtkSMSettings.h>
#include <vtkSMStringVectorProperty.h>

vtkStandardNewMacro(vtkSMConfigurableArrayListDomain);

static constexpr auto* SETTING_NAME = ".settings.ThemysSettings.ArrayNames";

//-----------------------------------------------------------------------------
void vtkSMConfigurableArrayListDomain::PrintSelf(ostream& o_stream,
                                                 vtkIndent indent)
{
  this->Superclass::PrintSelf(o_stream, indent);
}

//-----------------------------------------------------------------------------
int vtkSMConfigurableArrayListDomain::SetDefaultValues(
    vtkSMProperty* prop, bool /*use_unchecked_values*/)
{
  auto* svp = vtkSMStringVectorProperty::SafeDownCast(prop);
  if (svp == nullptr)
  {
    return 0;
  }

  if (this->GetNumberOfStrings() == 0)
  {
    return 1;
  }

  vtkSMSettings* setting = vtkSMSettings::GetInstance();

  const auto nbElems = setting->GetSettingNumberOfElements(SETTING_NAME);
  const auto nbElemsPerCommand = svp->GetNumberOfElementsPerCommand();
  std::vector<std::string> values;
  for (int idx = 0; idx < nbElems; idx += nbElemsPerCommand)
  {
    auto newName = setting->GetSettingAsString(SETTING_NAME, idx, "NoName");
    auto oldNames =
        setting->GetSettingAsString(SETTING_NAME, idx + 1, "NoName");
    std::istringstream stream;
    stream.str(oldNames);
    std::string name;
    while (std::getline(stream, name, ';'))
    {
      values.push_back(name);
      values.push_back(newName);
    }
  }

  svp->SetElements(values);

  return 1;
}
