/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkCEACellDataToPointData.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

  =========================================================================*/
#include "vtkCEACellDataToPointData.h"

#include <algorithm>
#include <array> // for array
#include <cassert>
#include <cmath>
#include <functional>
#include <limits>
#include <ostream> // for basic_ostream, operator<<
#include <set>
#include <string> // for basic_string, char_traits
#include <vector> // for vector

#include <vtkAbstractArray.h> // IWYU pragma: keep
#include <vtkAbstractCellLinks.h>
#include <vtkArrayDispatch.txx>     // for Dispatch2SameValueType
#include <vtkArrayListTemplate.h>   // For processing attribute data
#include <vtkArrayListTemplate.txx> // for ArrayList::AddArrays
#include <vtkCell.h>
#include <vtkCellArray.h> // for vtkCellArray
#include <vtkCellData.h>
#include <vtkCellLinks.h>     // for vtkCellLinks
#include <vtkCellTypes.h>     // for vtkCellTypes
#include <vtkDataArray.h>     // for vtkDataArray
#include <vtkDataArrayMeta.h> // for GetAPIType
#include <vtkDataArrayRange.h>
#include <vtkDataArrayTupleRange_Generic.h> // for operator!=
#include <vtkDataArrayValueRange_Generic.h> // for operator+, operator!=
#include <vtkDataSet.h>
#include <vtkDataSetAttributes.h> // for vtkDataSetAttributes
#include <vtkIdList.h>
#include <vtkIndent.h> // for operator<<, vtkIndent
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkNew.h>
#include <vtkObjectFactory.h>
#include <vtkOptions.h> // for VTK_USE_64BIT_IDS
#include <vtkPointData.h>
#include <vtkPointSet.h> // for vtkPointSet
#include <vtkPolyData.h> // for vtkPolyData
#include <vtkSMPTools.h>
#include <vtkSmartPointer.h>
#include <vtkStaticCellLinks.h>
#include <vtkStaticCellLinksTemplate.h>   // for vtkStaticCellLinksTemp...
#include <vtkStaticCellLinksTemplate.txx> // for vtkStaticCellLinksTemp...
#include <vtkStreamingDemandDrivenPipeline.h>
#include <vtkStructuredGrid.h>
#include <vtkUniformGrid.h>
#include <vtkUnsignedIntArray.h>
#include <vtkUnstructuredGrid.h>

constexpr const vtkIdType VTK_MAX_CELLS_PER_POINT{4096};

VTK_ABI_NAMESPACE_BEGIN
vtkObjectFactoryNewMacro(vtkCEACellDataToPointData)

    namespace
{

  //------------------------------------------------------------------------------
  // Optimized code for vtkUnstructuredGrid/vtkPolyData. It's waaaay faster than
  // the more general path.
  template <typename TCellLinks> struct UnstructuredDataCD2PD {
  private:
    TCellLinks* Links;
    ArrayList Arrays;

  public:
    UnstructuredDataCD2PD(vtkIdType numPts, vtkCellData* inDA,
                          vtkPointData* outDA, TCellLinks* links)
        : Links(links)
    {
      this->Arrays.AddArrays(numPts, inDA, outDA);
    }

    void operator()(vtkIdType beginPointId, vtkIdType endPointId)
    {
      for (vtkIdType pointId = beginPointId; pointId < endPointId; ++pointId)
      {
        const vtkIdType ncells{this->Links->GetNcells(pointId)};
        if (ncells > 0)
        {
          auto cells = this->Links->GetCells(pointId);
          this->Arrays.Average(ncells, cells, pointId);
        }
      }
    }
  };

  //------------------------------------------------------------------------------
  // Take care of dispatching to the functor using an abstract cell links.
  void FastUnstructuredDataACL(vtkIdType numPts, vtkAbstractCellLinks * links,
                               vtkCellData * cfl, vtkPointData * point_data)
  {
    assert(links != nullptr);
    if (auto* staticCellLinks = vtkStaticCellLinks::SafeDownCast(links))
    {
      UnstructuredDataCD2PD<vtkStaticCellLinks> cd2pd(numPts, cfl, point_data,
                                                      staticCellLinks);
      vtkSMPTools::For(0, numPts, cd2pd);
    } else // vtkCellLinks
    {
      auto* cellLinks = vtkCellLinks::SafeDownCast(links);
      UnstructuredDataCD2PD<vtkCellLinks> cd2pd(numPts, cfl, point_data,
                                                cellLinks);
      vtkSMPTools::For(0, numPts, cd2pd);
    }
  }

  //------------------------------------------------------------------------------
  // Take care of dispatching to the functor using a static cell links template
  // instance.
  template <typename TInput>
  void FastUnstructuredDataSCLT(vtkIdType connectivitySize, TInput * input,
                                vtkCellData * cfl, vtkPointData * point_data)
  {
    const auto numberOfPoints = input->GetNumberOfPoints();
    const auto numberOfCells = input->GetNumberOfCells();
    auto linksType = vtkAbstractCellLinks::ComputeType(
        numberOfPoints - 1, numberOfCells - 1, connectivitySize);
    // build the appropriate static cell links template instance
    if (linksType == vtkAbstractCellLinks::STATIC_CELL_LINKS_USHORT)
    {
      using TCellLinks = vtkStaticCellLinksTemplate<unsigned short>;
      TCellLinks cellLinks;
      cellLinks.BuildLinks(input);
      UnstructuredDataCD2PD<TCellLinks> cd2pd(numberOfPoints, cfl, point_data,
                                              &cellLinks);
      vtkSMPTools::For(0, numberOfPoints, cd2pd);
    }
#ifdef VTK_USE_64BIT_IDS
    else if (linksType == vtkAbstractCellLinks::STATIC_CELL_LINKS_UINT)
    {
      using TCellLinks = vtkStaticCellLinksTemplate<unsigned int>;
      TCellLinks cellLinks;
      cellLinks.BuildLinks(input);
      UnstructuredDataCD2PD<TCellLinks> cd2pd(numberOfPoints, cfl, point_data,
                                              &cellLinks);
      vtkSMPTools::For(0, numberOfPoints, cd2pd);
    }
#endif
    else
    {
      using TCellLinks = vtkStaticCellLinksTemplate<vtkIdType>;
      TCellLinks cellLinks;
      cellLinks.BuildLinks(input);
      UnstructuredDataCD2PD<TCellLinks> cd2pd(numberOfPoints, cfl, point_data,
                                              &cellLinks);
      vtkSMPTools::For(0, numberOfPoints, cd2pd);
    }
  }

  //------------------------------------------------------------------------------
  // Helper template function that implements the major part of the algorithm
  // which will be expanded by the vtkTemplateMacro. The template function is
  // provided so that coverage test can cover this function. This approach is
  // slow: it's non-threaded; uses a slower vtkDataSet API; and most
  // unfortunately, accommodates the ContributingCellOption which is not a
  // common workflow.
  struct Spread {
    static constexpr const vtkIdType s_abort_interval_ratio{10};
    static constexpr const vtkIdType s_abort_interval_min{1000};

    template <typename SrcArrayT, typename DstArrayT>
    // NOLINTNEXTLINE(readability-function-cognitive-complexity)
    void operator()(SrcArrayT* const srcarray, DstArrayT* const dstarray,
                    vtkDataSet* const src, vtkUnsignedIntArray* const num,
                    vtkIdType ncells, vtkIdType npoints, vtkIdType ncomps,
                    int highestCellDimension,
                    vtkCEACellDataToPointData* filter) const
    {
      // Both arrays will have the same value type:
      using T = vtk::GetAPIType<SrcArrayT>;

      // zero initialization
      std::fill_n(vtk::DataArrayValueRange(dstarray).begin(), npoints * ncomps,
                  T(0));

      const auto srcTuples = vtk::DataArrayTupleRange(srcarray);
      auto dstTuples = vtk::DataArrayTupleRange(dstarray);
      vtkIdType checkAbortInterval{0};

      // accumulate
      if (filter->GetContributingCellOption() !=
          vtkCEACellDataToPointData::Patch)
      {
        vtkNew<vtkIdList> pointIds;
        checkAbortInterval =
            std::min(ncells / s_abort_interval_ratio + 1, s_abort_interval_min);
        for (vtkIdType cid = 0; cid < ncells; ++cid)
        {
          if (cid % checkAbortInterval == 0 && filter->CheckAbort())
          {
            break;
          }
          const int dimension =
              vtkCellTypes::GetDimension(src->GetCellType(cid));
          if (dimension >= highestCellDimension)
          {
            const auto srcTuple = srcTuples[cid];
            src->GetCellPoints(cid, pointIds);
            for (vtkIdType i = 0, nb_ids = pointIds->GetNumberOfIds();
                 i < nb_ids; ++i)
            {
              const vtkIdType ptId = pointIds->GetId(i);
              auto dstTuple = dstTuples[ptId];
              // accumulate cell data to point data <==> point_data += cell_data
              std::transform(srcTuple.cbegin(), srcTuple.cend(),
                             dstTuple.cbegin(), dstTuple.begin(),
                             std::plus<T>());
            }
          }
        }
        // average

        checkAbortInterval = std::min(npoints / s_abort_interval_ratio + 1,
                                      s_abort_interval_min);
        for (vtkIdType pid = 0; pid < npoints; ++pid)
        {
          if (pid % checkAbortInterval == 0 && filter->CheckAbort())
          {
            break;
          }
          // guard against divide by zero
          if (unsigned int const denom = num->GetValue(pid))
          {
            // divide point data by the number of cells using it <==>
            // point_data /= denum
            auto dstTuple = dstTuples[pid];
            std::transform(dstTuple.cbegin(), dstTuple.cend(), dstTuple.begin(),
                           [denom](const auto num) { return num / denom; });
          }
        }
      } else
      { // compute over cell patches
        vtkNew<vtkIdList> cellsOnPoint;
        std::vector<T> data(4 * ncomps);
        checkAbortInterval = std::min(npoints / s_abort_interval_ratio + 1,
                                      s_abort_interval_min);
        for (vtkIdType pid = 0; pid < npoints; ++pid)
        {
          if (pid % checkAbortInterval == 0 && filter->CheckAbort())
          {
            break;
          }
          std::fill(data.begin(), data.end(), 0);
          std::array<T, 4> numPointCells{0, 0, 0, 0};
          // Get all cells touching this point.
          src->GetPointCells(pid, cellsOnPoint);
          const vtkIdType numPatchCells = cellsOnPoint->GetNumberOfIds();
          for (vtkIdType pc = 0; pc < numPatchCells; pc++)
          {
            const vtkIdType cellId = cellsOnPoint->GetId(pc);
            const int cellDimension = src->GetCell(cellId)->GetCellDimension();
            numPointCells.at(cellDimension) += 1;
            const auto srcTuple = srcTuples[cellId];
            for (int comp = 0; comp < ncomps; comp++)
            {
              data[comp + ncomps * cellDimension] += srcTuple[comp];
            }
          }
          auto dstTuple = dstTuples[pid];
          for (int dimension = 3; dimension >= 0; dimension--)
          {
            if (numPointCells.at(dimension))
            {
              for (int comp = 0; comp < ncomps; comp++)
              {
                dstTuple[comp] = data[comp + dimension * ncomps] /
                                 numPointCells.at(dimension);
              }
              break;
            }
          }
        }
      }
    }
  };

} // end anonymous namespace

//----------------------------------------------------------------------------
// Implementation support
class vtkCEACellDataToPointData::Internals
{
private:
  static constexpr const vtkIdType s_max_cells_per_node{8};

public:
  std::set<std::string> CellDataArrays;

  // Special traversal algorithm for vtkUniformGrid and vtkRectilinearGrid to
  // support blanking points will not have more than 8 cells for either of these
  // data sets
  template <typename T>
  int InterpolatePointDataWithMask(vtkCEACellDataToPointData* filter, T* input,
                                   vtkDataSet* output)
  {
    vtkNew<vtkIdList> allCellIds;
    allCellIds->Allocate(s_max_cells_per_node);
    vtkNew<vtkIdList> cellIds;
    cellIds->Allocate(s_max_cells_per_node);

    const vtkIdType numberOfPoints = input->GetNumberOfPoints();

    vtkCellData* inCD = input->GetCellData();
    vtkPointData* outPD = output->GetPointData();

    // Copy all existing cell fields into a temporary cell data array,
    // unless the SelectCellDataArrays option is active.
    vtkNew<vtkCellData> processedCellData;
    if (!filter->GetProcessAllArrays())
    {
      for (const auto& name : this->CellDataArrays)
      {
        vtkAbstractArray* array = inCD->GetAbstractArray(name.c_str());
        if (!array)
        {
          vtkWarningWithObjectMacro(filter, "cell data array name not found.");
          continue;
        }
        processedCellData->AddArray(array);
      }
    } else
    {
      processedCellData->ShallowCopy(inCD);
    }

    outPD->InterpolateAllocate(processedCellData, numberOfPoints);

    std::array<double, s_max_cells_per_node> weights{};

    bool abort = false;
    const vtkIdType progressInterval = numberOfPoints / 20 + 1;
    for (vtkIdType ptId = 0; ptId < numberOfPoints && !abort; ptId++)
    {
      if (!(ptId % progressInterval))
      {
        filter->UpdateProgress(static_cast<double>(ptId) / numberOfPoints);
        abort = filter->CheckAbort();
      }
      input->GetPointCells(ptId, allCellIds);
      cellIds->Reset();
      // Only consider cells that are not masked:
      for (vtkIdType cId = 0; cId < allCellIds->GetNumberOfIds(); ++cId)
      {
        const vtkIdType curCell = allCellIds->GetId(cId);
        if (input->IsCellVisible(curCell))
        {
          cellIds->InsertNextId(curCell);
        }
      }

      const vtkIdType numCells = cellIds->GetNumberOfIds();

      if (numCells > 0 && numCells <= s_max_cells_per_node)
      {
        assert(0 < numCells);
        assert(numCells < std::pow(10, std::numeric_limits<double>::digits10));
        const double weight = 1.0 / static_cast<double>(numCells);
        for (vtkIdType cellId = 0; cellId < numCells; cellId++)
        {
          weights.at(cellId) = weight;
        }
        outPD->InterpolatePoint(processedCellData, ptId, cellIds,
                                weights.data());
      } else
      {
        // TODO Log debug mode numCells > 8
        outPD->NullData(ptId);
      }
    }

    return 1;
  }
};

//------------------------------------------------------------------------------
// Instantiate object so that cell data is not passed to output.
vtkCEACellDataToPointData::vtkCEACellDataToPointData()
    : PassCellData{false},
      ContributingCellOption{vtkCEACellDataToPointData::All},
      WeightCellOption{vtkCEACellDataToPointData::Standard},
      BoundaryConditionPoint{vtkCEACellDataToPointData::AXIS_NONE},
      BoundaryConditionPoint2{vtkCEACellDataToPointData::AXIS_NONE},
      AxisAlignment{0.}, AxisAlignment2{0.},
      AbsoluteErrorEpsilonOnAxisAlignment{0.},
      ProcessAllArrays{true}, PieceInvariant{true},
      Implementation{new Internals()}
{
}

//------------------------------------------------------------------------------
vtkCEACellDataToPointData::~vtkCEACellDataToPointData()
{
  delete this->Implementation;
}

//------------------------------------------------------------------------------
void vtkCEACellDataToPointData::AddCellDataArray(const char* name)
{
  if (name == nullptr)
  {
    vtkErrorMacro("name cannot be null.");
    return;
  }

  this->Implementation->CellDataArrays.insert(std::string(name));
  this->Modified();
}

//------------------------------------------------------------------------------
void vtkCEACellDataToPointData::RemoveCellDataArray(const char* name)
{
  if (name == nullptr)
  {
    vtkErrorMacro("name cannot be null.");
    return;
  }

  this->Implementation->CellDataArrays.erase(name);
  this->Modified();
}

//------------------------------------------------------------------------------
void vtkCEACellDataToPointData::ClearCellDataArrays()
{
  if (!this->Implementation->CellDataArrays.empty())
  {
    this->Modified();
  }
  this->Implementation->CellDataArrays.clear();
}

//------------------------------------------------------------------------------
vtkIdType vtkCEACellDataToPointData::GetNumberOfCellArraysToProcess()
{
  return static_cast<vtkIdType>(this->Implementation->CellDataArrays.size());
}

//------------------------------------------------------------------------------
int vtkCEACellDataToPointData::RequestData(vtkInformation* /*request*/,
                                           vtkInformationVector** inputVector,
                                           vtkInformationVector* outputVector)
{
  vtkDataSet* input = vtkDataSet::GetData(inputVector[0]);
  vtkDataSet* output = vtkDataSet::GetData(outputVector);

  vtkDebugMacro(<< "Mapping cell data to point data");

  if (this->WeightCellOption == vtkCEACellDataToPointData::Standard)
  {
    // Special traversal algorithm for unstructured data such as vtkPolyData
    // and vtkUnstructuredGrid.
    // IsA return a vtkTypeBool let's keep its use as a true bool
    // NOLINTNEXTLINE(readability-implicit-bool-conversion)
    if (input->IsA("vtkUnstructuredGrid") || input->IsA("vtkPolyData"))
    {
      return this->RequestDataForUnstructuredData(nullptr, inputVector,
                                                  outputVector);
    }
  }
  // Not special traversal algorithm for unstructured data such as vtkPolyData
  // and vtkUnstructuredGrid when this mesh build on regular structured grid.
  // (2D Quad or 3D Hexahedron)

  // First, copy the input to the output as a starting point
  output->CopyStructure(input);

  vtkPointData* inPD = input->GetPointData();
  vtkCellData* inCD = input->GetCellData();
  vtkPointData* outPD = output->GetPointData();
  vtkCellData* outCD = output->GetCellData();

  // Pass the point data first. The fields and attributes
  // which also exist in the cell data of the input will
  // be over-written during CopyAllocate
  outPD->PassData(inPD);
  outPD->CopyFieldOff(vtkDataSetAttributes::GhostArrayName());

  if (input->GetNumberOfPoints() < 1)
  {
    vtkDebugMacro(<< "No input point data!");
    return 1;
  }

  // Do the interpolation, taking care of masked cells if needed.
  vtkStructuredGrid* sGrid = vtkStructuredGrid::SafeDownCast(input);
  vtkUniformGrid* uniformGrid = vtkUniformGrid::SafeDownCast(input);
  int result{0};
  if (sGrid != nullptr && sGrid->HasAnyBlankCells())
  {
    result =
        this->Implementation->InterpolatePointDataWithMask(this, sGrid, output);
  } else if (uniformGrid != nullptr && uniformGrid->HasAnyBlankCells())
  {
    result = this->Implementation->InterpolatePointDataWithMask(
        this, uniformGrid, output);
  } else
  {
    result = this->InterpolatePointData(input, output);
  }

  if (result == 0)
  {
    return 0;
  }

  if (!this->PassCellData)
  {
    outCD->CopyAllOff();
    outCD->CopyFieldOn(vtkDataSetAttributes::GhostArrayName());
  }
  outCD->PassData(inCD);

  return 1;
}

//------------------------------------------------------------------------------
int vtkCEACellDataToPointData::RequestUpdateExtent(
    vtkInformation* /*request*/, vtkInformationVector** inputVector,
    vtkInformationVector* outputVector)
{
  if (!this->PieceInvariant)
  {
    // I believe the default input update extent
    // is set to the input update extent.
    return 1;
  }

  // Technically, this code is only correct for pieces extent types.  However,
  // since this class is pretty inefficient for data types that use 3D extents,
  // we'll punt on the ghost levels for them, too.

  // get the info objects
  vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation* outInfo = outputVector->GetInformationObject(0);

  const int piece =
      outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER());
  const int numPieces =
      outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES());
  int ghostLevels = outInfo->Get(
      vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_GHOST_LEVELS());

  if (numPieces > 1)
  {
    ++ghostLevels;
  }

  inInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER(), piece);
  inInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES(),
              numPieces);
  inInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_GHOST_LEVELS(),
              ghostLevels);
  inInfo->Set(vtkStreamingDemandDrivenPipeline::EXACT_EXTENT(), 1);

  return 1;
}

//------------------------------------------------------------------------------
void vtkCEACellDataToPointData::PrintSelf(ostream& o_stream, vtkIndent indent)
{
  this->Superclass::PrintSelf(o_stream, indent);

  o_stream << indent
           << "PassCellData: " << (this->PassCellData ? "On\n" : "Off\n");
  o_stream << indent
           << "ContributingCellOption: " << this->ContributingCellOption
           << endl;
  o_stream << indent << "WeightCellOption: " << this->WeightCellOption << endl;
  o_stream << indent
           << "PieceInvariant: " << (this->PieceInvariant ? "On\n" : "Off\n");
}

//----------------------------------------------------------------------------
// In general the method below is quite slow due to ContributingCellOption
// considerations. If the ContributingCellOption is "All", and the dataset
// type is unstructured, then a threaded, tuned approach is used.
// NOLINTNEXTLINE(readability-function-cognitive-complexity)
int vtkCEACellDataToPointData::RequestDataForUnstructuredData(
    vtkInformation* /*request*/, vtkInformationVector** inputVector,
    vtkInformationVector* outputVector)
{
  vtkPointSet* input = vtkPointSet::GetData(inputVector[0]);
  vtkPointSet* output = vtkPointSet::GetData(outputVector);

  const vtkIdType numberOfCells = input->GetNumberOfCells();
  const vtkIdType numberOfPoints = input->GetNumberOfPoints();
  if (numberOfCells < 1 || numberOfPoints < 1)
  {
    vtkDebugMacro(<< "No input data!");
    return 1;
  }

  // Begin by performing the tasks common to both the slow and fast paths.

  // First, copy the input structure (geometry and topology) to the output as
  // a starting point.
  output->CopyStructure(input);

  vtkCellData* inCD = input->GetCellData();
  vtkPointData* outPD = output->GetPointData();
  vtkCellData* outCD = output->GetCellData();

  // Pass the point data first. The fields and attributes which also exist in
  // the cell data of the input will be over-written during CopyAllocate
  outPD->CopyGlobalIdsOff();
  outPD->PassData(input->GetPointData());
  outPD->CopyFieldOff(vtkDataSetAttributes::GhostArrayName());

  // Copy all existing cell fields into a temporary cell data array,
  // unless the SelectCellDataArrays option is active.
  vtkNew<vtkCellData> processedCellData;
  if (!this->ProcessAllArrays)
  {
    for (const auto& name : this->Implementation->CellDataArrays)
    {
      vtkAbstractArray* array = inCD->GetAbstractArray(name.c_str());
      if (array == nullptr)
      {
        vtkWarningMacro("cell data array name not found.");
        continue;
      }
      processedCellData->AddArray(array);
    }
  } else
  {
    processedCellData->ShallowCopy(inCD);
  }

  // Remove all fields that are not a data array.
  for (int fid = processedCellData->GetNumberOfArrays(); fid != 0; fid--)
  {
    if (vtkDataArray::FastDownCast(processedCellData->GetAbstractArray(fid)) ==
        nullptr)
    {
      processedCellData->RemoveArray(fid);
    }
  }

  outPD->InterpolateAllocate(processedCellData, numberOfPoints);

  // Pass the input cell data to the output as appropriate.
  if (!this->PassCellData)
  {
    outCD->CopyAllOff();
    outCD->CopyFieldOn(vtkDataSetAttributes::GhostArrayName());
  }
  outCD->PassData(inCD);

  // Now perform the averaging operation.

  // Use a much faster approach for the "All" ContributingCellOption, and
  // unstructured datasets. A common workflow requiring maximum performance.
  if (this->ContributingCellOption == vtkCEACellDataToPointData::All)
  {
    if (auto* uGrid = vtkUnstructuredGrid::SafeDownCast(input))
    {
      if (uGrid->GetLinks() != nullptr) // if links are present use them
      {
        uGrid->BuildLinks(); // ensure links are up to date
        FastUnstructuredDataACL(numberOfPoints, uGrid->GetLinks(),
                                processedCellData, outPD);
      } else // otherwise create links with the minimum size
      {
        const vtkIdType connectivitySize =
            uGrid->GetCells()->GetNumberOfConnectivityIds();
        FastUnstructuredDataSCLT(connectivitySize, uGrid, processedCellData,
                                 outPD);
      }
      return 1;
    }
    // polydata
    auto* polyData = vtkPolyData::SafeDownCast(input);
    if (polyData->GetLinks() != nullptr) // if links are present use them
    {
      polyData->BuildLinks(); // ensure links are up to date
      FastUnstructuredDataACL(numberOfPoints, polyData->GetLinks(),
                              processedCellData, outPD);
    } else // otherwise create links with the minimum size
    {
      auto* verts = polyData->GetVerts();
      auto* lines = polyData->GetLines();
      auto* polys = polyData->GetPolys();
      auto* strips = polyData->GetStrips();
      vtkIdType connectivitySize = 0;
      connectivitySize +=
          verts != nullptr ? verts->GetNumberOfConnectivityIds() : 0;
      connectivitySize +=
          lines != nullptr ? lines->GetNumberOfConnectivityIds() : 0;
      connectivitySize +=
          polys != nullptr ? polys->GetNumberOfConnectivityIds() : 0;
      connectivitySize +=
          strips != nullptr ? strips->GetNumberOfConnectivityIds() : 0;
      FastUnstructuredDataSCLT(connectivitySize, polyData, processedCellData,
                               outPD);
    }
    return 1;
  } // fast path

  // If necessary, begin the slow, more general path.

  // To a large extent the loops immediately following are a serial version
  // of BuildLinks() found in vtkUnstructuredGrid and vtkPolyData. The code
  // below could be threaded if necessary. Count the number of cells
  // associated with each point. If we are doing patches though we will do
  // that later on.
  vtkSmartPointer<vtkUnsignedIntArray> num;
  int highestCellDimension = 0;
  if (this->ContributingCellOption != vtkCEACellDataToPointData::Patch)
  {
    num = vtkSmartPointer<vtkUnsignedIntArray>::New();
    num->SetNumberOfValues(numberOfPoints);
    num->FillValue(0);
    if (this->ContributingCellOption == vtkCEACellDataToPointData::DataSetMax)
    {
      const int maxDimension = input->IsA("vtkPolyData") == 1 ? 2 : 3;
      for (vtkIdType i = 0; i < numberOfCells; i++)
      {
        const int dim = vtkCellTypes::GetDimension(input->GetCellType(i));
        if (dim > highestCellDimension)
        {
          highestCellDimension = dim;
          if (highestCellDimension == maxDimension)
          {
            break;
          }
        }
      }
    }
    vtkNew<vtkIdList> pids;
    for (vtkIdType cid = 0; cid < numberOfCells; ++cid)
    {
      if (input->GetCell(cid)->GetCellDimension() >= highestCellDimension)
      {
        input->GetCellPoints(cid, pids);
        for (vtkIdType i = 0, nb_ids = pids->GetNumberOfIds(); i < nb_ids; ++i)
        {
          vtkIdType const pid = pids->GetId(i);
          num->SetValue(pid, num->GetValue(pid) + 1);
        }
      }
    }
  }

  const auto nfields = processedCellData->GetNumberOfArrays();
  int fid = 0;
  auto transformer = [this, &fid, nfields, numberOfPoints, input, num,
                      numberOfCells,
                      highestCellDimension](vtkAbstractArray* aa_srcarray,
                                            vtkAbstractArray* aa_dstarray) {
    // update progress and check for an abort request.
    this->UpdateProgress((fid + 1.0) / nfields);
    ++fid;

    vtkDataArray* const srcarray = vtkDataArray::FastDownCast(aa_srcarray);
    vtkDataArray* const dstarray = vtkDataArray::FastDownCast(aa_dstarray);
    if (srcarray != nullptr && dstarray != nullptr)
    {
      dstarray->SetNumberOfTuples(numberOfPoints);
      vtkIdType const ncomps = srcarray->GetNumberOfComponents();

      const Spread worker;
      using Dispatcher = vtkArrayDispatch::Dispatch2SameValueType;
      if (!Dispatcher::Execute(srcarray, dstarray, worker, input, num,
                               numberOfCells, numberOfPoints, ncomps,
                               highestCellDimension, this))
      { // fallback for unknown arrays:
        worker(srcarray, dstarray, input, num, numberOfCells, numberOfPoints,
               ncomps, highestCellDimension, this);
      }
    }
  };

  // Cell field list constructed from the filtered cell data array
  vtkDataSetAttributes::FieldList cfl(1);
  cfl.InitializeFieldList(processedCellData);
  if (processedCellData != nullptr && outPD != nullptr)
  {
    cfl.TransformData(0, processedCellData, outPD, transformer);
  }

  return 1; // slow path
}

//------------------------------------------------------------------------------
// NOLINTNEXTLINE(readability-function-cognitive-complexity)
int vtkCEACellDataToPointData::InterpolatePointData(vtkDataSet* input,
                                                    vtkDataSet* output)
{
  vtkNew<vtkIdList> cellIds;
  cellIds->Allocate(VTK_MAX_CELLS_PER_POINT);

  const vtkIdType numberOfPoints = input->GetNumberOfPoints();

  vtkCellData* inCD = input->GetCellData();
  vtkPointData* outPD = output->GetPointData();

  // Copy all existing cell fields into a temporary cell data array,
  // unless the SelectCellDataArrays option is active.
  vtkNew<vtkCellData> processedCellData;
  if (!this->ProcessAllArrays)
  {
    for (const auto& name : this->Implementation->CellDataArrays)
    {
      vtkAbstractArray* array = inCD->GetAbstractArray(name.c_str());
      if (array == nullptr)
      {
        vtkWarningMacro("cell data array name not found.");
        continue;
      }
      processedCellData->AddArray(array);
    }
  } else
  {
    processedCellData->ShallowCopy(inCD);
  }

  outPD->InterpolateAllocate(processedCellData, numberOfPoints);

  std::array<double, VTK_MAX_CELLS_PER_POINT> weights{};
  std::array<double, VTK_MAX_CELLS_PER_POINT> weightsStandard{};

  if (this->WeightCellOption != vtkCEACellDataToPointData::Standard)
  {
    const double weight = 1.0 / this->WeightCellOption;
    std::fill(weights.begin(), weights.end(), weight);
  }

  bool abort = false;
  const vtkIdType progressInterval = numberOfPoints / 20 + 1;
  for (vtkIdType ptId = 0; ptId < numberOfPoints && !abort; ptId++)
  {
    if (ptId % progressInterval == 0)
    {
      assert(numberOfPoints <
             std::pow(10, std::numeric_limits<double>::digits10));
      assert(ptId < std::pow(10, std::numeric_limits<double>::digits10));
      this->UpdateProgress(static_cast<double>(ptId) /
                           static_cast<double>(numberOfPoints));
      abort = this->CheckAbort();
    }

    input->GetPointCells(ptId, cellIds);
    const vtkIdType numCells = cellIds->GetNumberOfIds();

    bool boundaryCondition = true;
    bool boundaryCondition2 = true;
    if (this->WeightCellOption != vtkCEACellDataToPointData::Standard)
    {
      std::array<double, 3> coord{};
      if (this->BoundaryConditionPoint != vtkCEACellDataToPointData::AXIS_NONE
        || this->BoundaryConditionPoint2 != vtkCEACellDataToPointData::AXIS_NONE)
      {
        input->GetPoint(ptId, coord.data());
      }

      auto epsilon = this->AbsoluteErrorEpsilonOnAxisAlignment;
      // when reaches second condition, boundary != AXIS_NONE, so no need
      //  to add this test
      auto needs_std_interpolation = [&coord, &epsilon](const int boundary,
                                                        const double value) {
        return (boundary != vtkCEACellDataToPointData::AXIS_NONE &&
                (std::fabs(coord.at(boundary) - value) < epsilon));
      };
      boundaryCondition = needs_std_interpolation(this->BoundaryConditionPoint,
        this->AxisAlignment);
      boundaryCondition2 = needs_std_interpolation(this->BoundaryConditionPoint2,
        this->AxisAlignment2);
    }

    if (!boundaryCondition && !boundaryCondition2)
    {
      // use the value corresponding to WeightCellOption
      outPD->InterpolatePoint(processedCellData, ptId, cellIds, weights.data());
    } else if (numCells > 0 && numCells <= VTK_MAX_CELLS_PER_POINT)
    {
      // use the number of cells for this point
      assert(numCells < std::pow(10, std::numeric_limits<double>::digits10));
      const double weight = 1.0 / static_cast<double>(numCells);
      for (vtkIdType cellId = 0; cellId < numCells; cellId++)
      {
        weightsStandard.at(cellId) = weight;
      }
      outPD->InterpolatePoint(processedCellData, ptId, cellIds,
                              weightsStandard.data());
    } else
    {
      // TODO Log debug mode numCells > VTK_MAX_CELLS_PER_POINT
      outPD->NullData(ptId);
    }
  }

  return 1;
}
VTK_ABI_NAMESPACE_END
