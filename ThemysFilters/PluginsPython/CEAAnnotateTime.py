from paraview.util.vtkAlgorithm import *
from paraview.modules.vtkPVVTKExtensionsFiltersPython import vtkPythonAnnotationFilter

try:
    from CEAWrapping.HerculeServicesReader import vtkHerculeServicesReader

    CEA_WRAPPING = True
except:
    CEA_WRAPPING = False


@smproxy.filter(
    name="CEAAnnotateTime",
    label="CEA AnnotateTime",
)
# This hint allows to keep the current RenderView visible and avoid opening a SpreadSheetView
@smhint.xml(
    """
    <Visibility replace_input='0'/><OutputPort index='0' name='Output-0' type='text'/>
    """
)
# This hint add icon
@smhint.xml(
    """
    <ShowInMenu icon=":/Themys/pqAnnotateTime.svg" />
    """
)
@smproperty.input(name="Input")
@smproperty.xml(
    """
    <Documentation
        long_help="Annotate the render view with time value, time step index and source registration name"
        short_help="Display time informations and source registration name">
    </Documentation>
    """
)
@smdomain.datatype(dataTypes=["vtkDataObject"])
@smdomain.xml(
    """
    <InputArrayDomain attribute_type="field" name="input_normal" number_of_components="1"/>
    """
)
class CEAAnnotateTime(VTKPythonAlgorithmBase):
    def __init__(self):
        super().__init__(
            nInputPorts=1,
            nOutputPorts=1,
            inputType="vtkDataObject",
            outputType="vtkTable",
        )
        self.DesiredTimeUnit = 0
        self.TimeVariable = None
        self.TimeIndexVariable = None
        self.UseRegistrationName = False
        self._Algorithm = vtkPythonAnnotationFilter()

    def GetSource(self):
        """
        Return the source at the base of the pipeline
        See https://discourse.vtk.org/t/how-to-get-a-name-of-the-previous-filter-in-the-pipeline/14659
        """

        def RecurseOnPipeline(algorithm):
            if algorithm.GetNumberOfInputPorts() == 0:
                return algorithm
            conn = algorithm.GetInputConnection(0, 0)
            if not conn:
                return algorithm
            return RecurseOnPipeline(conn.GetProducer())

        src = RecurseOnPipeline(self)
        return src

    def GetRegistrationName(self):
        """
        Return the registration name of the source if it is an HerculeServicesReader, None otherwise
        """
        if not self.UseRegistrationName:
            return None

        src = self.GetSource()
        if CEA_WRAPPING:
            src = vtkHerculeServicesReader.SafeDownCast(src)
            if src is not None:
                return src.GetRegistrationName()

        return None

    def BuildExpression(self):
        """
        Build the expression that will be passed to _Algorithm for further evaluation

        It has the following form:
        'Temps %f $\mu$s (#%d)' % (vtkFixedTimeValue*1e6, vtkFixedTimeStep)
        """
        match self.DesiredTimeUnit:
            case 0:
                time_unit_str = "$\mu$s"
                time_unit_multiplier = "1e6"
            case 1:
                time_unit_str = "ns"
                time_unit_multiplier = "1e9"
            case 2:
                time_unit_str = "ps"
                time_unit_multiplier = "1e12"
            case _:
                time_unit_str = "$\mu$s"

        src_name = self.GetRegistrationName()
        prefix = ""
        if src_name:
            prefix = f"{src_name}"

        values = []
        titles = []
        if self.TimeVariable != "None":
            titles.append(f"Temps %f {time_unit_str}")
            values.append(f"{self.TimeVariable}*{time_unit_multiplier}")

        if self.TimeIndexVariable != "None":
            titles.append(f"Indice %d")
            values.append(f"{self.TimeIndexVariable}")

        return f"'{prefix}{' : ' if prefix and (titles or values) else ''}{' '.join(titles)}' %({', '.join(values)})"

    # Here is an enum property to expose desired unit
    @smproperty.intvector(name="DesiredTimeUnit", default_values=0)
    @smdomain.xml(
        """
        <EnumerationDomain name="enum">
            <Entry value="0" text="MicroSecond" />
            <Entry value="1" text="NanoSecond" />
            <Entry value="2" text="PicoSecond" />
        </EnumerationDomain>
        """
    )
    def SetDesiredTimeUnit(self, value):
        if self.DesiredTimeUnit != value:
            self.DesiredTimeUnit = value
            self.Modified()

    @smproperty.stringvector(
        name="TimeVariable",
        default_values="vtkFixedTimeValue",
        number_of_elements=1,
    )
    @smdomain.xml(
        """
        <ArrayListDomain attribute_type="Scalars" none_string="None" name="array_list">
            <RequiredProperties>
                <Property function="Input" name="Input"/>
            </RequiredProperties>
        </ArrayListDomain>
        """
    )
    def SetTimeVariable(self, var_name):
        if self.TimeVariable != var_name:
            self.TimeVariable = var_name
            self.Modified()

    @smproperty.stringvector(
        name="TimeIndexVariable",
        default_values="vtkFixedTimeStep",
        number_of_elements=1,
    )
    @smdomain.xml(
        """
        <ArrayListDomain attribute_type="Scalars" none_string="None" name="array_list">
            <RequiredProperties>
                <Property function="Input" name="Input"/>
            </RequiredProperties>
        </ArrayListDomain>
        """
    )
    def SetTimeIndexVariable(self, var_name):
        if self.TimeIndexVariable != var_name:
            self.TimeIndexVariable = var_name
            self.Modified()

    @smproperty.intvector(
        name="UseRegistrationName",
        number_of_elements=1,
        default_values=1,
    )
    @smdomain.xml(
        """
        <BooleanDomain name="bool" />
    """
    )
    def SetUseRegistrationName(self, val):
        if self.SetUseRegistrationName != val:
            self.UseRegistrationName = val
            self.Modified()

    def RequestData(self, request, inInfo, outInfo):
        inData = self.GetInputData(inInfo, 0, 0)
        outData = self.GetOutputData(outInfo, 0)

        self._Algorithm.SetInputData(inData)
        self._Algorithm.SetExpression(self.BuildExpression())
        self._Algorithm.Update()

        outData.DeepCopy(self._Algorithm.GetOutput())

        return 1
