from paraview.util.vtkAlgorithm import *
from vtkmodules.vtkCommonDataModel import (
    vtkDataObjectTree,
    vtkMultiBlockDataSet,
    vtkPolyData,
    vtkUnstructuredGrid,
    vtkCell,
)
from vtkmodules.vtkFiltersCore import vtkFeatureEdges, vtkCleanPolyData
from vtkmodules.vtkFiltersExtraction import vtkExtractBlock
from vtkmodules.vtkFiltersGeometry import vtkGeometryFilter
from vtkmodules.vtkFiltersParallelDIY2 import vtkGhostCellsGenerator
from vtkmodules.vtkIOXML import vtkXMLMultiBlockDataWriter
from paraview.modules.vtkPVVTKExtensionsMisc import vtkReductionFilter


# CEAWriterDat functions
# --------------------------------------------------------------
def NodeToDatStr(i_node: int, cell: vtkCell, vtkpd: vtkPolyData):
    """
    Writes the coordinates of the node into a string

    :param i_node: node index
    :param cell: the cell the node belongs to
    :param vtkpd: polydata object the cell belongs to
    :return: the string holding coordinates
    """
    i_point = cell.GetPointId(i_node)
    p_coords = vtkpd.GetPoint(i_point)
    try:
        return f"{p_coords[0]:10.10f} {p_coords[1]:10.10f} {p_coords[2]:10.10f}\n"
    except IndexError:
        return f"{p_coords[0]:10.10f} {p_coords[1]:10.10f}\n"


def CellToDatStr(i_cell: int, vtkpd: vtkPolyData) -> str:
    """
    Writes the coordinates of each point defining the cell into a string

    :param i_cell: cell index
    :param vtkpd: polydata object the cell belongs to
    :return: the string holding coordinates
    """
    cell = vtkpd.GetCell(i_cell)
    nb_nodes = cell.GetNumberOfPoints()
    if nb_nodes <= 1:
        return ""

    return "".join(NodeToDatStr(i_node, cell, vtkpd) for i_node in range(nb_nodes))


def PolyDataToDatStr(vtkpd: vtkPolyData, cell_separator="&\n") -> str:
    """
    Writes the coordinates of each point defining a cell in the polydata in argument
    in a string

    :param vtkpd: polydata object to write
    :param cell_separator: string that separates each cell
    :return: the string holding points coordinates
    """
    return cell_separator.join(
        CellToDatStr(i_cell, vtkpd) for i_cell in range(vtkpd.GetNumberOfCells())
    )


def RecursiveCustomWriter(inData: vtkDataObjectTree, file, polydata_sep="&\n") -> None:
    """
    Append the coordinates of each nodes of each vtkPolyData into the file in argument

    :param inData: the vtkDataObjectTree to write
    :param file: opened file to write
    :param polydata_sep: the string that separates each polydata
    """
    blockIterator = inData.NewIterator()
    blockIterator.GoToFirstItem()

    while not blockIterator.IsDoneWithTraversal():
        currentBlock = blockIterator.GetCurrentDataObject()

        # Current block is another dataObjectTree
        if isinstance(currentBlock, vtkDataObjectTree):
            RecursiveCustomWriter(currentBlock, file, polydata_sep)
        # Current block is a leaf <=> vtkPolyData
        elif isinstance(currentBlock, vtkPolyData):
            data = PolyDataToDatStr(currentBlock) + polydata_sep
            file.write(data)

        blockIterator.GoToNextItem()


def ApplyCustomWriter(filename: str, inData: vtkDataObjectTree) -> None:
    """
    Open the file in argument and recursively writes in it

    :param filename: path to the file to write
    :param inData: the vtkDataObjectTree to write
    """
    with open(filename, "w") as fd_out:
        RecursiveCustomWriter(inData, fd_out)


# --------------------------------------------------------------
# End CEAWriterDat functions


# Educational Filter
# Takes a vtkDataObjectTree and apply a pipeline to each extracted block
# before writing the output as a vtm or dat file
@smproxy.filter(name="EducationalFilter")
@smproperty.input(name="Input")
@smdomain.datatype(dataTypes=["vtkDataObjectTree"])
class EducationalFilter(VTKPythonAlgorithmBase):
    def __init__(self):
        super().__init__(
            nInputPorts=1,
            nOutputPorts=1,
            inputType="vtkDataObjectTree",
            outputType="vtkDataObjectTree",
        )
        # In order to expose block selection, we need to keep this filter
        self._extractBlock = vtkExtractBlock()
        # File name should be the name of the file without extension
        # The full file name will be set automatically depending writing mode
        self._fileName = None
        self._fullFileName = None
        # Whether to write the output or not, in custom dat file or vtm file
        # Custom writer can write any vtkDataObjectTree
        # Other writers could be set to handle not only vtkMultiBlockDataSet
        # but vtkPartitionedDataSet or vtkPartitionedDataSetCollection
        # 0: Do not write | 1: Use CEAWriterDat | 2: Use multiblock XML writer
        self._writingMode = 0

    # Here we implemented a RequestDataObject that returns an instance of the input data type
    def RequestDataObject(self, request, inInfo, outInfo):
        inData = self.GetInputData(inInfo, 0, 0)
        outData = self.GetOutputData(outInfo, 0)
        assert inData is not None
        if outData is None or (not outData.IsA(inData.GetClassName())):
            outData = inData.NewInstance()
            outInfo.GetInformationObject(0).Set(outData.DATA_OBJECT(), outData)
        return super().RequestDataObject(request, inInfo, outInfo)

    # We expose the BlockIndices property the same way as it is exposed in the XML definition
    # of the ExtractBlock filter in ParaView
    @smproperty.xml(
        """
        <IntVectorProperty clean_command="RemoveAllIndices"
                    command="AddIndex"
                    name="BlockIndices"
                    number_of_elements_per_command="1"
                    panel_visibility="default"
                    repeat_command="1">
            <Hints>
                <WidgetHeight number_of_rows="20"/>
            </Hints>
            <Documentation>
                This property lists the ids of the blocks to extract from the input dataObjectTree dataset.
            </Documentation>
            <CompositeTreeDomain name="tree">
                <RequiredProperties>
                <Property function="Input" name="Input"/>
                </RequiredProperties>
            </CompositeTreeDomain>
        </IntVectorProperty>
    """
    )
    def AddIndex(self, id):
        self._extractBlock.AddIndex(id)
        self.Modified()

    def RemoveAllIndices(self):
        self._extractBlock.RemoveAllIndices()
        self.Modified()

    # We define a property to let user choose filename (without extension)
    # Hint is needed to specify a non already existing file
    @smproperty.stringvector(name="FileName", default_values="my_file")
    @smdomain.filelist()
    @smhint.xml(
        """
        <AcceptAnyFile/>
    """
    )
    def SetFileName(self, fname):
        if self._fileName != fname:
            self._fileName = fname
            self.Modified()

    # Here is an enum property to expose writing mode
    # We may not write or write using
    # the custom writer (CEADataWriter)
    # or the XML Multiblock writer (in ACSII)
    @smproperty.intvector(name="WritingMode", default_values=0)
    @smdomain.xml(
        """
        <EnumerationDomain name="enum">
            <Entry value="0" text="Do not write" />
            <Entry value="1" text="Use CEAWriterDat" />
            <Entry value="2" text="Use multiblock XML writer" />
        </EnumerationDomain>
    """
    )
    def SetWritingMode(self, value):
        self._writingMode = value
        self.Modified()

    # Recursive call to allow pipeline application
    # on sub-blocks of the dataObjectTree
    def RecursivePipeline(self, inData, outData):
        blockIterator = inData.NewIterator()
        blockIterator.GoToFirstItem()

        while not blockIterator.IsDoneWithTraversal():
            currentBlock = blockIterator.GetCurrentDataObject()

            # Current block is another dataObjectTree
            if isinstance(currentBlock, vtkDataObjectTree):
                self.RecursivePipeline(currentBlock, outData)
            # Current block is a leaf
            else:
                output = self.ApplyPipeline(currentBlock)
                if output is not None:
                    outData.SetDataSet(blockIterator, output)

            blockIterator.GoToNextItem()

    # Application of the pipeline to a specific block
    # returning its output data object
    # or None if type is not vtkPolyData or vtkUnstructuredGrid
    def ApplyPipeline(self, block):
        # Pipeline filters declaration
        # First pipeline filter needs to set its input data
        # Next ones need to set its input connection like a normal pipeline
        geometryFilter = vtkGeometryFilter()
        cleanPolyData = vtkCleanPolyData()
        ghostCellsGenerator = vtkGhostCellsGenerator()
        featureEdges = vtkFeatureEdges()
        reductionFilter = vtkReductionFilter()

        # Block is a 2D object
        if isinstance(block, vtkPolyData):
            cleanPolyData.SetInputData(block)
        # Block is a 3D object, so turn it into a 2D polydata first
        elif isinstance(block, vtkUnstructuredGrid):
            # vtkGeometryFilter is better than vtkDataSetSurfaceFilter for performances
            # If data is incompatible, it will fallback on a vtkDataSetSurfaceFilter
            geometryFilter.SetInputData(block)
            cleanPolyData.SetInputConnection(geometryFilter.GetOutputPort())
        # Otherwise, do not treat this block
        else:
            print("[SKIP] Block is not a poly data nor an unstructured grid")
            return None

        # vtkGhostCellsGenerator needs a clean poly data input to work properly
        ghostCellsGenerator.SetInputConnection(cleanPolyData.GetOutputPort())
        featureEdges.SetInputConnection(ghostCellsGenerator.GetOutputPort())
        reductionFilter.SetInputConnection(featureEdges.GetOutputPort())
        # Last pipeline filter needs to call Update to run the full pipeline
        reductionFilter.Update()

        return reductionFilter.GetOutputDataObject(0)

    # Function handling automatic file naming
    # Could handle more writing modes, or a smarter default mode
    def GetFileNameExtension(self):
        if self._writingMode == 2:
            return vtkXMLMultiBlockDataWriter().GetDefaultFileExtension()
        return "dat"

    # Main logic
    def RequestData(self, request, inInfo, outInfo):
        inData = self.GetInputData(inInfo, 0, 0)
        outData = self.GetOutputData(outInfo, 0)

        # Set full file name with extension depending on writer mode
        self._fullFileName = self._fileName + "." + self.GetFileNameExtension()

        # To iterate over wanted blocks,
        # we need to directly apply our first filter
        self._extractBlock.SetInputData(inData)
        self._extractBlock.Update()
        dataObjectTree = self._extractBlock.GetOutputDataObject(0)

        # Output is built with same structure than filter output
        # in order to easily set output with iterators
        outData.CopyStructure(dataObjectTree)
        # Call the recursive pipeline
        self.RecursivePipeline(dataObjectTree, outData)

        # Writes the output using custom dat writer
        if self._writingMode == 1:
            ApplyCustomWriter(self._fullFileName, outData)
        # Writes the output using the default writer,
        # unless no valid block was detected
        elif self._writingMode == 2:
            if not isinstance(outData, vtkMultiBlockDataSet):
                print(
                    "[ERROR] Trying to write vtkMultiBlockDataSet but "
                    + outData.GetClassName()
                    + " was given."
                )
            elif outData.GetNumberOfBlocks() > 0:
                xmlWriter = vtkXMLMultiBlockDataWriter()
                xmlWriter.SetInputData(outData)
                xmlWriter.SetFileName(self._fullFileName)
                xmlWriter.SetDataModeToAscii()
                xmlWriter.Write()

        return 1
