from paraview.util.vtkAlgorithm import *

from vtkmodules.vtkCommonDataModel import (
    vtkDataObjectTree,
    vtkDataObject,
    vtkUnstructuredGrid,
)
from vtkmodules.vtkFiltersParallelDIY2 import vtkGhostCellsGenerator

from paraview.modules.vtkPVVTKExtensionsFiltersGeneral import vtkIsoVolume

# new module for ParaView-specific decorators.
# from paraview.util.vtkAlgorithm import smproxy, smproperty, smdomain

from CEAWrapping.CEACellDataToPointData import vtkCEACellDataToPointData


# /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\
# WARNING : the name of the class for PV is based on the **LABEL**, not the name
@smproxy.filter(name="CEASmoothInterfacesFilter", label="CEA Smooth Interfaces Filter")
@smproperty.input(name="Input")
@smdomain.datatype(dataTypes=["vtkDataObjectTree"])
@smdomain.xml(
    """
    <InputArrayDomain attribute_type="cell" name="input_normal" number_of_components="1"/>
    """
)
class CEASmoothInterfacesFilter(VTKPythonAlgorithmBase):
    def __init__(self):
        super().__init__(
            nInputPorts=1,
            nOutputPorts=1,
            inputType="vtkDataObjectTree",
            outputType="vtkDataObjectTree",
        )
        self.ArrayToProcess = None
        self.Threshold = [None, None]
        self.WeightCell = 0
        self.BoundaryConditionPoint = 3
        self.AxisAlignmentBoundaryConditionPoint = 0.0
        self.BoundaryConditionPoint2 = 3
        self.AxisAlignmentBoundaryConditionPoint2 = 0.0
        self.AbsoluteErrorEpsilonOnAxisAlignment = 1.0e-7

    def RequestDataObject(self, request, inInfo, outInfo):
        inData = self.GetInputData(inInfo, 0, 0)
        outData = self.GetOutputData(outInfo, 0)
        assert inData is not None
        if outData is None or (not outData.IsA(inData.GetClassName())):
            outData = inData.NewInstance()
            outInfo.GetInformationObject(0).Set(outData.DATA_OBJECT(), outData)
        return super().RequestDataObject(request, inInfo, outInfo)

    def RequestData(self, request, inInfo, outInfo):
        """
        Tips :
        - Avoid create new type between input and output.
        - Do we need a "composite aware" filter ?
            - a composite aware filter is a filter which executes on the whole composite structure instead of on leaves of the composite
        - For a non composite aware filter, no need to loop over each leaf, pv do it for us
        """
        inData = self.GetInputData(inInfo, 0, 0)

        # vtkConvertToPartitionedDataSetCollection as first filter is not needed according to KW
        ghostCellsGenerator = vtkGhostCellsGenerator()
        ghostCellsGenerator.SetInputDataObject(inData)

        cell2pointFilter = vtkCEACellDataToPointData()
        cell2pointFilter.SetInputConnection(ghostCellsGenerator.GetOutputPort())
        cell2pointFilter.PassCellDataOn()
        cell2pointFilter.PieceInvariantOn()
        cell2pointFilter.ProcessAllArraysOff()
        cell2pointFilter.AddCellDataArray(self.ArrayToProcess)
        cell2pointFilter.SetWeightCellOption(self.WeightCell)
        cell2pointFilter.SetBoundaryConditionPoint(self.BoundaryConditionPoint)
        cell2pointFilter.SetAxisAlignment(self.AxisAlignmentBoundaryConditionPoint)
        cell2pointFilter.SetBoundaryConditionPoint2(self.BoundaryConditionPoint2)
        cell2pointFilter.SetAbsoluteErrorEpsilonOnAxisAlignment(
            self.AbsoluteErrorEpsilonOnAxisAlignment
        )

        isoVolFilter = vtkIsoVolume()
        isoVolFilter.SetInputConnection(cell2pointFilter.GetOutputPort())
        isoVolFilter.SetInputArrayToProcess(
            0, 0, 0, vtkDataObject.FIELD_ASSOCIATION_POINTS, self.ArrayToProcess
        )
        isoVolFilter.ThresholdBetween(self.Threshold[0], self.Threshold[1])
        isoVolFilter.Update()

        outData = self.GetOutputData(outInfo, 0)
        outData.ShallowCopy(isoVolFilter.GetOutput())

        return 1

    @smproperty.stringvector(name="InputArray", default_values="FracVol")
    @smdomain.xml(
        """<ArrayListDomain name="array_list" attribute_type="Scalars" >
            <RequiredProperties>
               <Property name="Input" function="Input"/>
            </RequiredProperties>
         </ArrayListDomain>
      """
    )
    def SetInputArrayToProcess(self, arrayName):
        if arrayName != self.ArrayToProcess:
            self.ArrayToProcess = arrayName
            self.Modified()

    @smproperty.doublevector(name="Minimum", default_values=0.5)
    @smdomain.doublerange(min=0.0, max=1.0)
    def SetMinimum(self, x):
        if x != self.Threshold[0]:
            self.Threshold[0] = x
            self.Modified()

    @smproperty.doublevector(name="Maximum", default_values=1.0001)
    @smdomain.doublerange(min=0.0, max=1.1)
    def SetMaximum(self, x):
        if x != self.Threshold[1]:
            self.Threshold[1] = x
            self.Modified()

    @smproperty.intvector(name="WeightCellOption", default_values=0)
    @smdomain.xml(
        """
        <EnumerationDomain name="enum">
          <Entry text="Standard" value="0" />
          <Entry text="Quad" value="4" />
          <Entry text="Hexahedron" value="8" />
        </EnumerationDomain>
        """
    )
    def SetWeightCellOption(self, v):
        if v != self.WeightCell:
            self.WeightCell = v
            self.Modified()

    @smproperty.intvector(
        name="BoundaryConditionPoint", panel_visibility="advanced", default_values=3
    )
    @smdomain.xml(
        """
        <EnumerationDomain name="enum">
          <Entry text="None" value="3" />
          <Entry text="X Axis" value="0" />
          <Entry text="Y Axis" value="1" />
          <Entry text="Z Axis" value="2" />
        </EnumerationDomain>
        """
    )
    def SetBoundaryConditionPoint(self, v):
        if v != self.BoundaryConditionPoint:
            self.BoundaryConditionPoint = v
            self.Modified()

    @smproperty.doublevector(
        name="AxisAlignmentBoundaryConditionPoint",
        panel_visibility="advanced",
        default_values=0.0,
    )
    def SetAxisAlignmentBoundaryConditionPoint(self, v):
        if v != self.AxisAlignmentBoundaryConditionPoint:
            self.AxisAlignmentBoundaryConditionPoint = v
            self.Modified()

    @smproperty.intvector(
        name="BoundaryConditionPoint2", panel_visibility="advanced", default_values=3
    )
    @smdomain.xml(
        """
        <EnumerationDomain name="enum">
          <Entry text="None" value="3" />
          <Entry text="X Axis" value="0" />
          <Entry text="Y Axis" value="1" />
          <Entry text="Z Axis" value="2" />
        </EnumerationDomain>
        """
    )
    def SetBoundaryConditionPoint2(self, v):
        if v != self.BoundaryConditionPoint2:
            self.BoundaryConditionPoint2 = v
            self.Modified()

    @smproperty.doublevector(
        name="AxisAlignmentBoundaryConditionPoint2",
        panel_visibility="advanced",
        default_values=0.0,
    )
    def SetAxisAlignmentBoundaryConditionPoint2(self, v):
        if v != self.AxisAlignmentBoundaryConditionPoint2:
            self.AxisAlignmentBoundaryConditionPoint2 = v
            self.Modified()

    @smproperty.doublevector(
        name="AbsoluteErrorEpsilonOnAxisAlignment",
        panel_visibility="advanced",
        default_values=1.0e-7,
    )
    def SetAbsoluteErrorEpsilonOnAxisAlignment(self, v):
        if v != self.AbsoluteErrorEpsilonOnAxisAlignment:
            self.AbsoluteErrorEpsilonOnAxisAlignment = v
            self.Modified()
