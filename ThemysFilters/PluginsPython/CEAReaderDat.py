"""CEA DAT Python reader
The vtkPolyData instance name is file name."""

# ------------------------------------------------------------------------------

from paraview.util.vtkAlgorithm import *
import numpy as np
import vtk

# ------------------------------------------------------------------------------


@smproxy.reader(
    name="CEAReaderDAT",
    label="CEA Reader DAT (Python)",
    extensions="dat",
    file_description="DAT files",
)
class CEAReaderDAT(VTKPythonAlgorithmBase):
    """A reader that reads a DAT file."""

    def __init__(self):
        VTKPythonAlgorithmBase.__init__(
            self, nInputPorts=0, nOutputPorts=1, outputType="vtkPolyData"
        )
        self.m_filename = None
        self.m_eps = None

    @smproperty.stringvector(name="FileName")
    @smdomain.filelist()
    @smhint.filechooser(extensions="dat", file_description="DAT files")
    def SetFileName(self, _filename):
        """Specify filename for the file to read."""
        if self.m_filename != _filename:
            self.m_filename = _filename
            self.Modified()

    @smproperty.doublevector(name="ZEpsilon", default_values=[0.0001])
    def SetZEpsilon(self, _eps):
        """Specify filename for the file to read."""
        if self.m_eps != _eps:
            self.m_eps = _eps
            self.Modified()

    # Information only property adds a read-only widget in the properties panel.
    @smproperty.stringvector(
        name="Version", panel_visibility="advanced", information_only="1"
    )
    def GetVersion(self):
        return "1.0 (c)JBL 2021"

    def RequestInformation(self, request, inInfoVec, outInfoVec):
        executive = self.GetExecutive()
        outInfo = outInfoVec.GetInformationObject(0)
        return 1

    def RequestData(self, request, inInfoVec, outInfoVec):
        if self.m_filename is None:
            raise RuntimeError("No filename specified")

        points = vtk.vtkPoints()
        cells = vtk.vtkCellArray()

        with open(self.m_filename, "r") as f:
            line = f.readline()
            firstPoint = 0
            lastPoint = 0
            while line:
                if line[0] == "&" or line[0] == "#" or line == "" or line == "\n":
                    n_points = lastPoint - firstPoint
                    if n_points != 0:
                        polyline = vtk.vtkPolyLine()
                        polyline.GetPointIds().SetNumberOfIds(n_points)
                        for i in range(n_points):
                            polyline.GetPointIds().SetId(i, firstPoint + i)
                        cells.InsertNextCell(polyline)
                    firstPoint = lastPoint
                else:
                    words = line.split()
                    try:
                        p = [float(words[0]), float(words[1]), float(words[2])]
                    except:
                        p = [float(words[0]), float(words[1]), self.m_eps]
                    points.InsertNextPoint(p)
                    lastPoint += 1
                line = f.readline()

        n_points = lastPoint - firstPoint
        if n_points != 0:
            polyline = vtk.vtkPolyLine()
            polyline.GetPointIds().SetNumberOfIds(n_points)
            for i in range(n_points):
                polyline.GetPointIds().SetId(i, firstPoint + i)
            cells.InsertNextCell(polyline)

        from vtkmodules.numpy_interface import dataset_adapter as dsa

        output = dsa.WrapDataObject(vtk.vtkPolyData.GetData(outInfoVec))
        output.SetPoints(points)
        output.SetLines(cells)
        return 1
