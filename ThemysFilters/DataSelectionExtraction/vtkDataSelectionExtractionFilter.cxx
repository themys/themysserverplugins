#include "vtkDataSelectionExtractionFilter.h"

#include <algorithm>

#include <vtkAlgorithm.h> // for vtkAlgorithm
#include <vtkCellData.h>
#include <vtkCompositeDataIterator.h>
#include <vtkCompositeDataSet.h>
#include <vtkDataArray.h>
#include <vtkDataObject.h> // for vtkDataObject
#include <vtkDataSet.h>
#include <vtkGenericDataArray.txx> // for vtkGenericDataArray::I...
#include <vtkIdTypeArray.h>        // for vtkIdTypeArray
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkNew.h>
#include <vtkObject.h>        // for vtkObject
#include <vtkObjectFactory.h> // for vtkStandardNewMacro
#include <vtkPVExtractSelection.h>
#include <vtkPointData.h>
#include <vtkSelection.h>
#include <vtkSelectionNode.h>
#include <vtkSmartPointer.h> // for vtkSmartPointer, TakeS...
#include <vtkStreamingDemandDrivenPipeline.h>
#include <vtkVariantInlineOperators.h> // for vtkVariant::operator==

class vtkAlgorithmOutput;

vtkStandardNewMacro(vtkDataSelectionExtractionFilter);

//----------------------------------------------------------------------------
vtkDataSelectionExtractionFilter::vtkDataSelectionExtractionFilter()
    : InputData{nullptr}
{
  this->SetNumberOfInputPorts(2);
}

//----------------------------------------------------------------------------
void vtkDataSelectionExtractionFilter::SetSelectionConnection(
    vtkAlgorithmOutput* algOutput)
{
  this->SetInputConnection(1, algOutput);
}

//------------------------------------------------------------------------------
int vtkDataSelectionExtractionFilter::FillInputPortInformation(
    int port, vtkInformation* info)
{
  if (port == 0)
  {
    info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
    info->Append(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(),
                 "vtkCompositeDataSet");
  } else if (port == 1)
  {
    info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkSelection");
    info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
  } else
  {
    return 0;
  }

  return 1;
}

//------------------------------------------------------------------------------
int vtkDataSelectionExtractionFilter::SetupOutput(vtkInformation* inInfo,
                                                  vtkInformation* outInfo)
{
  const vtkIdType piece =
      outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER());
  const int numPieces =
      outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES());
  vtkDataObject* input = inInfo->Get(vtkDataObject::DATA_OBJECT());

  if (vtkCompositeDataSet* hdInput = vtkCompositeDataSet::SafeDownCast(input))
  {
    this->InputData = hdInput;
    hdInput->Register(this);
    return 1;
  }
  if (vtkDataSet* dsInput = vtkDataSet::SafeDownCast(input))
  {
    this->InputIsADataSet = true;
    auto mbds = vtkSmartPointer<vtkMultiBlockDataSet>::New();
    mbds->SetNumberOfBlocks(numPieces);
    mbds->SetBlock(piece, dsInput);
    this->InputData = mbds;
    mbds->Register(this);

    return 1;
  }

  vtkErrorMacro("This filter cannot handle input of type: "
                << (input ? input->GetClassName() : "(none)"));
  return 0;
}

//----------------------------------------------------------------------------
void vtkDataSelectionExtractionFilter::StoreBlockName(
    std::vector<std::string>& blockNames, vtkMultiBlockDataSet* mbds)
{
  const auto childNbBlock = mbds->GetNumberOfBlocks();
  for (int i = 0; i < childNbBlock; i++)
  {
    if (vtkMultiBlockDataSet* child =
            vtkMultiBlockDataSet::SafeDownCast(mbds->GetBlock(i)))
    {
      vtkDataSelectionExtractionFilter::StoreBlockName(blockNames, child);
    } else
    {
      const auto* name =
          mbds->GetMetaData(i)->Get(vtkMultiBlockDataSet::NAME());
      if (mbds->GetBlock(i)->GetNumberOfElements(vtkDataSet::POINT) != 0)
      {
        blockNames.emplace_back(name);
      }
    }
  }
}

//----------------------------------------------------------------------------
int vtkDataSelectionExtractionFilter::UpdateSelectedValuesFromIDSelection(
    vtkSelectionNode* currentNode, vtkIdType compositeIndex,
    vtkSelectionNode::SelectionField& selectionField)
{
  vtkDataSet* currentBlock{nullptr};
  if (this->InputIsADataSet)
  {
    auto* mbds = vtkMultiBlockDataSet::SafeDownCast(this->InputData);
    currentBlock = vtkDataSet::SafeDownCast(mbds->GetBlock(compositeIndex));
  } else
  {
    currentBlock =
        vtkDataSet::SafeDownCast(this->InputData->GetDataSet(compositeIndex));
  }

  if (currentBlock == nullptr)
  {
    return 1;
  }

  return this->FillActiveSelectionValues(currentBlock, currentNode,
                                         selectionField);
}

//----------------------------------------------------------------------------
int vtkDataSelectionExtractionFilter::
    UpdateSelectedValuesFromQueryOrFrustrumSelection(
        vtkSelectionNode* currentNode, vtkCompositeDataSet* rawCD,
        vtkSelectionNode::SelectionField& selectionField)
{
  auto iter = vtk::TakeSmartPointer(rawCD->NewIterator());
  for (iter->InitTraversal(); iter->IsDoneWithTraversal() == 0;
       iter->GoToNextItem())
  {
    vtkDataSet* currentBlock = vtkDataSet::SafeDownCast(
        rawCD->GetDataSet(iter->GetCurrentFlatIndex()));
    if (currentBlock == nullptr)
    {
      continue;
    }

    if (this->FillActiveSelectionValues(currentBlock, currentNode,
                                        selectionField) == 0)
    {
      return 0;
    }
  }

  return 1;
}

//----------------------------------------------------------------------------
int vtkDataSelectionExtractionFilter::FillActiveSelectionValues(
    vtkDataSet* currentBlock, vtkSelectionNode* currentNode,
    vtkSelectionNode::SelectionField& selectionField)
{
  // Extract the desired data array in the block
  int association = vtkDataObject::FIELD_ASSOCIATION_NONE;
  vtkDataArray* array =
      this->GetInputArrayToProcess(0, currentBlock, association);
  if (array == nullptr)
  {
    vtkWarningMacro(<< "No array can be processed");
    return 0;
  }

  vtkNew<vtkPVExtractSelection> extractSelection;
  vtkNew<vtkSelection> selection;
  selection->AddNode(currentNode);
  extractSelection->SetInputData(0, currentBlock);
  extractSelection->SetInputData(1, selection);
  extractSelection->Update();
  vtkDataSet* selectedBlock =
      vtkDataSet::SafeDownCast(extractSelection->GetOutput());

  // Fill ActiveSelectionValues with the desired data array in the current
  // extracted selection
  vtkDataArray* selectedArray{nullptr};
  switch (association)
  {
  case vtkDataObject::FIELD_ASSOCIATION_CELLS:
    selectionField = vtkSelectionNode::CELL;
    selectedArray = selectedBlock->GetCellData()->GetArray(array->GetName());
    break;

  case vtkDataObject::FIELD_ASSOCIATION_POINTS:
    selectionField = vtkSelectionNode::POINT;
    selectedArray = selectedBlock->GetPointData()->GetArray(array->GetName());
    break;

  default:
    vtkWarningMacro("Unsupported field association");
    return 0;
  }

  if (selectedArray != nullptr)
  {
    for (vtkIdType i = 0; i < selectedArray->GetNumberOfValues(); i++)
    {
      this->ActiveSelectionValues.insert(selectedArray->GetVariantValue(i));
    }
  }

  return 1;
}

//----------------------------------------------------------------------------
// NOLINTNEXTLINE(readability-function-cognitive-complexity)
int vtkDataSelectionExtractionFilter::RequestData(
    vtkInformation* /*request*/, vtkInformationVector** inputVector,
    vtkInformationVector* outputVector)
{
  vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation* outInfo = outputVector->GetInformationObject(0);

  // Setup input/output to handle only a CompositeDataSet
  if (this->SetupOutput(inInfo, outInfo) == 0)
  {
    return 0;
  }

  vtkSelection* activeSelection = vtkSelection::GetData(inputVector[1], 0);
  vtkMultiBlockDataSet* output = vtkMultiBlockDataSet::SafeDownCast(
      outInfo->Get(vtkDataObject::DATA_OBJECT()));
  if (this->InputData == nullptr)
  {
    vtkErrorMacro(<< "Invalid input or output.");
    return 0;
  }
  if (output == nullptr || activeSelection == nullptr)
  {
    vtkErrorMacro(<< "Invalid input or output.");
    this->InputData->UnRegister(this);
    return 0;
  }

  const int numPieces =
      outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES());
  output->SetNumberOfBlocks(numPieces);

  this->ActiveSelectionValues.clear();

  // Used later if a SelectionNode is a query/frustrum selection
  vtkNew<vtkPVExtractSelection> extractRawData;
  extractRawData->SetInputData(0, this->InputData);
  extractRawData->SetInputData(1, activeSelection);
  extractRawData->Update();
  vtkCompositeDataSet* rawCD =
      vtkCompositeDataSet::SafeDownCast(extractRawData->GetOutput());

  // Search for each selection node what points/cells was selected in the good
  // block and fill ActiveSelectionValues with the values of the dataArray
  // choosen by the user
  vtkSelectionNode::SelectionField selectionField =
      vtkSelectionNode::NUM_FIELD_TYPES;
  for (int i = 0; i < activeSelection->GetNumberOfNodes(); i++)
  {
    auto* currentNode = activeSelection->GetNode(i);

    // Because we can't used the COMPOSITE_INDEX() information from a
    // query/frustrum we need to split the logic
    auto nodeType =
        currentNode->GetProperties()->Get(vtkSelectionNode::CONTENT_TYPE());
    if (nodeType == vtkSelectionNode::QUERY ||
        nodeType == vtkSelectionNode::FRUSTUM)
    {
      if (this->UpdateSelectedValuesFromQueryOrFrustrumSelection(
              currentNode, rawCD, selectionField) == 0)
      {
        break;
      }
    } else
    {
      const vtkIdType compositeIndex = currentNode->GetProperties()->Get(
          vtkSelectionNode::COMPOSITE_INDEX());

      if (this->UpdateSelectedValuesFromIDSelection(currentNode, compositeIndex,
                                                    selectionField) == 0)
      {
        break;
      }
    }
  }

  if (this->ActiveSelectionValues.empty())
  {
    this->InputData->UnRegister(this);
    vtkWarningMacro("No data can be extracted");
    return 0;
  }

  std::vector<std::string> blockNames;
  if (!this->InputIsADataSet)
  {
    if (vtkMultiBlockDataSet* mbds =
            vtkMultiBlockDataSet::SafeDownCast(this->InputData))
    {
      vtkDataSelectionExtractionFilter::StoreBlockName(blockNames, mbds);
    }
  }

  // Extract desired selection for each block and append it in the output
  int blockId = 0;
  auto iter = vtk::TakeSmartPointer(this->InputData->NewIterator());
  for (iter->InitTraversal(); iter->IsDoneWithTraversal() == 0;
       iter->GoToNextItem())
  {
    vtkDataSet* currentBlock =
        vtkDataSet::SafeDownCast(iter->GetCurrentDataObject());
    if (currentBlock == nullptr)
    {
      continue;
    }

    vtkDataArray* array = this->GetInputArrayToProcess(0, currentBlock);
    if (array == nullptr)
    {
      vtkWarningMacro(<< "No array can be processed");
      return 0;
    }

    // Find all matching indices
    vtkNew<vtkIdTypeArray> indices;
    for (vtkIdType i = 0; i < array->GetNumberOfValues(); i++)
    {
      const auto& var_value = array->GetVariantValue(i);
      if (std::any_of(
              this->ActiveSelectionValues.cbegin(),
              this->ActiveSelectionValues.cend(),
              [var_value](const auto& val) { return var_value == val; }))
      {
        indices->InsertNextValue(i);
      }
    }

    vtkNew<vtkPVExtractSelection> extractSelection;
    vtkNew<vtkSelection> selection;
    vtkNew<vtkSelectionNode> selectionNode;
    selectionNode->SetFieldType(selectionField);
    selectionNode->SetContentType(vtkSelectionNode::INDICES);
    selectionNode->SetSelectionList(indices);
    selection->AddNode(selectionNode);

    extractSelection->SetInputData(0, currentBlock);
    extractSelection->SetInputData(1, selection);
    extractSelection->Update();

    auto* newBlock = extractSelection->GetOutputDataObject(0);
    if (newBlock != nullptr &&
        newBlock->GetNumberOfElements(selectionField) != 0)
    {
      output->SetBlock(blockId, newBlock);
      if (blockId < blockNames.size())
      {
        output->GetMetaData(blockId)->Set(vtkCompositeDataSet::NAME(),
                                          blockNames[blockId]);
      }
    }
    blockId++;
  }

  this->InputData->UnRegister(this);
  return 1;
}
