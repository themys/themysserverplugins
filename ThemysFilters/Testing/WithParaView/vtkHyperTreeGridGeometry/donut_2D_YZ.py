"""
This test check that the vtkHyperTreeGridGeometry filters is functional when
working on a 2D HTG base.
It opens a 2D HTG base and applies the filter and then ensures the result is
a vtkPolyData
Remark: the image obtained with the filter is the same as the one without applying
the filter because a similar filter is used for rendering the HTG 2D object
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *

from paraview import servermanager as sm
from paraview.vtk.numpy_interface import dataset_adapter as dsa

from common import THEMYSSERVERPLUGINS_TESTING_PATH, launch_comparison

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

HTG_NAME = "donut_YZ"
# create a new 'HyperTreeGrid Reader'
donut_YZhtg = HyperTreeGridReader(
    registrationName=f"{HTG_NAME}.htg",
    FileNames=[
        f"{THEMYSSERVERPLUGINS_TESTING_PATH}/vtkHyperTreeGridGeometry/{HTG_NAME}.htg"
    ],
)

# get active view
renderView1 = GetActiveViewOrCreate("RenderView")

# show data in view
donut_YZhtgDisplay = Show(donut_YZhtg, renderView1, "HyperTreeGridRepresentation")

# trace defaults for the display properties.
donut_YZhtgDisplay.Representation = "Surface"

# reset view to fit data
renderView1.ResetCamera(False, 0.9)

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(donut_YZhtgDisplay, ("CELLS", "level"))

# rescale color and/or opacity maps used to include current data range
donut_YZhtgDisplay.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
donut_YZhtgDisplay.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'level'
levelLUT = GetColorTransferFunction("level")

# get opacity transfer function/opacity map for 'level'
levelPWF = GetOpacityTransferFunction("level")

# get 2D transfer function for 'level'
levelTF2D = GetTransferFunction2D("level")

# create a new 'vtk HTG Geometry'
vtkHTGGeometry1 = HyperTreeGridGeometryFilter(
    registrationName="vtkHTGGeometry1", Input=donut_YZhtg
)

# show data in view
vtkHTGGeometry1Display = Show(vtkHTGGeometry1, renderView1, "GeometryRepresentation")

# trace defaults for the display properties.
vtkHTGGeometry1Display.Representation = "Surface"

# hide data in view
Hide(donut_YZhtg, renderView1)

# show color bar/color legend
vtkHTGGeometry1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# ================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
# ================================================================

# get layout
layout1 = GetLayout()

# --------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
layout1.SetSize(1984, 1144)

# -----------------------------------
# saving camera placements for views

# current camera placement for renderView1
renderView1.InteractionMode = "2D"
renderView1.CameraPosition = [5.465355893763006, 0.5000000298023224, 0.0]
renderView1.CameraFocalPoint = [0.0, 0.5000000298023224, 0.0]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 1.414538193569175

launch_comparison(renderView1, baseline_relative_dir="vtkHyperTreeGridGeometry")

result = dsa.WrapDataObject(sm.Fetch(vtkHTGGeometry1))
assert result.GetClassName() == "vtkPolyData"
