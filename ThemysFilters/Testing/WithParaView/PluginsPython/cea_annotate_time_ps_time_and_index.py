# trace generated using paraview version 5.13.1-687-g8acca3c94c
# import paraview
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 13

#### import the simple module from the paraview
from paraview import servermanager as sm
from paraview.vtk.numpy_interface import dataset_adapter as dsa
from paraview.simple import *

from common import THEMYSSERVERPLUGINS_TESTING_PATH, launch_comparison

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XML PolyData Reader'
SRC_DATA = XMLPolyDataReader(
    registrationName="CustomSourceName",
    FileName=[
        f"{THEMYSSERVERPLUGINS_TESTING_PATH}/ContourWriter/MultiBlockDataSet/MBDS/MBDS_3.vtp"
    ],
)

# Properties modified on SRC_DATA
SRC_DATA.TimeArray = "None"

# get active view
RENDER_VIEW = GetActiveViewOrCreate("RenderView")

# show data in view
SRC_DATA_DISPLAY = Show(SRC_DATA, RENDER_VIEW, "GeometryRepresentation")

# trace defaults for the display properties.
SRC_DATA_DISPLAY.Representation = "Surface"

# reset view to fit data
RENDER_VIEW.ResetCamera(False, 0.9)

# changing interaction mode based on data extents
RENDER_VIEW.InteractionMode = "2D"
RENDER_VIEW.CameraPosition = [0.5, 0.3499999940395355, 3.35]
RENDER_VIEW.CameraFocalPoint = [0.5, 0.3499999940395355, 0.0]

# update the view to ensure updated data information
RENDER_VIEW.Update()

# set scalar coloring
ColorBy(SRC_DATA_DISPLAY, ("CELLS", "Milieu:Density"))

# rescale color and/or opacity maps used to include current data range
SRC_DATA_DISPLAY.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
SRC_DATA_DISPLAY.SetScalarBarVisibility(RENDER_VIEW, True)

# create a new 'CEA AnnotateTime'
ANNOTATED = CEAAnnotateTime(registrationName="CEAAnnotateTime1", Input=SRC_DATA)
ANNOTATED.DesiredTimeUnit = "PicoSecond"
ANNOTATED.TimeVariable = "vtkFixedTimeValue"
ANNOTATED.TimeIndexVariable = "vtkFixedTimeStep"
ANNOTATED.UseRegistrationName = True

# show data in view
ANNOTATED_DISPLAY = Show(ANNOTATED, RENDER_VIEW, "TextSourceRepresentation")

# update the view to ensure updated data information
RENDER_VIEW.Update()

# ================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
# ================================================================

# get layout
LAYOUT = GetLayout()

# --------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
LAYOUT.SetSize(800, 600)

# -----------------------------------
# saving camera placements for views

# current camera placement for renderView1
RENDER_VIEW.InteractionMode = "2D"
RENDER_VIEW.CameraPosition = [0.5, 0.3499999940395355, 3.35]
RENDER_VIEW.CameraFocalPoint = [0.5, 0.3499999940395355, 0.0]
RENDER_VIEW.CameraParallelScale = 0.6103277773685832

launch_comparison(RENDER_VIEW, baseline_relative_dir="PluginsPython")

database = dsa.WrapDataObject(sm.Fetch(ANNOTATED))
print(f"{database.GetRowData().GetArray(0).GetValue(0)}=")
assert (
    database.GetRowData().GetArray(0).GetValue(0)
    == "Temps 1000280412347.218872 ps Indice 5"
)
