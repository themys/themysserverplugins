"""
This module tests that CEASmoothIsoVolumeFilter is functional.

It loads a 2D vtkUnstructuredGrid object made of 4 mats and runs CEASmoothIsoVolumeFilter.
"""
# import paraview
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 13

#### import the simple module from the paraview
from paraview.simple import *

from common import THEMYSSERVERPLUGINS_TESTING_PATH, launch_comparison

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()


# create a new 'XML MultiBlock Data Reader'
vortex_vtm = XMLMultiBlockDataReader(
    registrationName="HDep-n=Temps_u=s.p-0001_000005.vtm",
    FileName=[
        f"{THEMYSSERVERPLUGINS_TESTING_PATH}/MaterialFilters/4MatsVortex/HDep-n=Temps_u=s.p-0001_000005.vtm"
    ],
)

# get active view
render_view = GetActiveViewOrCreate("RenderView")

# show data in view
vortex_vtmDisplay = Show(vortex_vtm, render_view, "GeometryRepresentation")

# trace defaults for the display properties.
vortex_vtmDisplay.Representation = "Surface"

smooth_interfaces = CEASmoothInterfacesFilter(
    registrationName="smooth_interfaces", Input=vortex_vtm
)

# Properties modified on smooth_interfaces
smooth_interfaces.InputArray = "vtkInterfaceFraction"
smooth_interfaces.Maximum = 1.2
smooth_interfaces.WeightCellOption = "Quad"

# show data in view
smooth_interfaces_display = Show(
    smooth_interfaces, render_view, "UnstructuredGridRepresentation"
)

# trace defaults for the display properties.
smooth_interfaces_display.Representation = "Surface"

# hide data in view
Hide(vortex_vtm, render_view)

# hide color bar/color legend
smooth_interfaces_display.SetScalarBarVisibility(render_view, False)

# Properties modified on smooth_interfaces_display
smooth_interfaces_display.BlockSelectors = [
    "/Root/M1/_1NE",
    "/Root/M1/_2NW",
    "/Root/M1/_3SE",
    "/Root/M1/_4SW",
]

# reset view to fit data bounds
render_view.ResetCamera(0.0, 1.0, 0.0, 1.0, 0.0, 0.0, True, 0.9)

# update the view to ensure updated data information
render_view.Update()

# ================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
# ================================================================

# get layout
layout1 = GetLayout()

# --------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
layout1.SetSize(800, 600)

# -----------------------------------
# saving camera placements for views

# current camera placement for render_view
render_view.InteractionMode = "2D"
render_view.CameraPosition = [0.5, 0.5, 2.7320508075688776]
render_view.CameraFocalPoint = [0.5, 0.5, 0.0]
render_view.CameraParallelScale = 0.5550408067378276

# remove orientation axes
render_view.OrientationAxesVisibility = 0

# update the view to ensure updated data information
render_view.Update()

launch_comparison(render_view, baseline_relative_dir="PluginsPython")
