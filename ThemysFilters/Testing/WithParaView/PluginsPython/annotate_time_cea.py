"""
This test open a database and applies the CEAAnnotateTimeFilter.
It compares the obtained image to a reference and also test the exact
value of the annotated text
"""
# import paraview
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 13

#### import the simple module from the paraview
from paraview import servermanager as sm
from paraview.vtk.numpy_interface import dataset_adapter as dsa
from paraview.simple import *

from common import THEMYSSERVERPLUGINS_TESTING_PATH, launch_comparison

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XML PolyData Reader'
SRC_DATA = XMLPolyDataReader(
    registrationName="Vortex_part",
    FileName=[
        f"{THEMYSSERVERPLUGINS_TESTING_PATH}/MaterialFilters/4MatsVortex/HDep-n=Temps_u=s.p-0001_000005/HDep-n=Temps_u=s.p-0001_000005_1_0.vtp"
    ],
)

# Properties modified on hDepnTemps_usp0001_000005_1_0vtp
SRC_DATA.TimeArray = "None"

# get active view
RENDER_VIEW = GetActiveViewOrCreate("RenderView")

# show data in view
SRC_DATA_DISPLAY = Show(SRC_DATA, RENDER_VIEW, "GeometryRepresentation")

# trace defaults for the display properties.
SRC_DATA_DISPLAY.Representation = "Surface"

# reset view to fit data
RENDER_VIEW.ResetCamera(False, 0.9)

# update the view to ensure updated data information
RENDER_VIEW.Update()

# set scalar coloring
ColorBy(SRC_DATA_DISPLAY, ("CELLS", "Milieu:Density"))

# rescale color and/or opacity maps used to include current data range
SRC_DATA_DISPLAY.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
SRC_DATA_DISPLAY.SetScalarBarVisibility(RENDER_VIEW, True)

# create a new 'Annotate Time CEA'
ANNOTATED = AnnotateTimeCEA(registrationName="CEAAnnotateTime1", Input=SRC_DATA)

# show data in view
ANNOTATED_DISPLAY = Show(ANNOTATED, RENDER_VIEW, "TextSourceRepresentation")

# update the view to ensure updated data information
RENDER_VIEW.Update()

# ================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
# ================================================================

# get layout
LAYOUT = GetLayout()

# --------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
LAYOUT.SetSize(800, 600)

# -----------------------------------
# saving camera placements for views

# current camera placement for renderView1
RENDER_VIEW.InteractionMode = "2D"
RENDER_VIEW.CameraPosition = [0.6500000059604645, 0.5, 2.358125450647677]
RENDER_VIEW.CameraFocalPoint = [0.6500000059604645, 0.5, 0.0]
RENDER_VIEW.CameraParallelScale = 0.6103277773685832

launch_comparison(RENDER_VIEW, baseline_relative_dir="PluginsPython")

database = dsa.WrapDataObject(sm.Fetch(ANNOTATED))
assert (
    database.GetRowData().GetArray(0).GetValue(0) == "Temps 1000280.412347 $\\mu$s (#5)"
)
