# Registers the pvpython (python) tests
if(${PARAVIEW_USE_PYTHON})
  set(PythonTestNames
      ceareaderdat
      ceareaderdat_2d
      educational_4matvortex_8procs
      annotate_time_cea
      annotate_time_cea_pointdata
      annotate_time_cea_celldata
      cea_annotate_time_mus_time_and_index
      cea_annotate_time_mus_time
      cea_annotate_time_mus_index
      cea_annotate_time_mus
      cea_annotate_time_ns_time_and_index
      cea_annotate_time_ps_time_and_index
      cea_smooth_interfaces_frac_vol2
      cea_smooth_interfaces_4matsvortex
      cea_smooth_interfaces_boundary_4matsvortex
  )

  # Necessary so that the common module is found. Necessitates also to copy each module test in the same directory
  set(_vtk_build_TEST_FILE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
  configure_file(${CMAKE_CURRENT_SOURCE_DIR}/../common.py.in ${_vtk_build_TEST_FILE_DIRECTORY}/common.py)

  foreach(test_name ${PythonTestNames})
    register_paraview_python_test(ThemysFilters ${test_name})
  endforeach()

endif()
