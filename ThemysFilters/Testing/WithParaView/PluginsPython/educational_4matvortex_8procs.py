"""
This module tests that the CEAEducational filter is functional.

It loads a vtkMultiBlockDataSet made of 4 mats (vortex) each of them being made
of 4 blocks (16 blocks), then apply the filter to obtain and save the contour using the
CEAdatWriter integrated in the filter.

Each material is made of 4 blocks because the database is derived from an Hercule
database that has been visualized with 4 pvservers and then saved into .vtm files

This is also the reason why in the obtained contour we can see the frontiers of the
4 subdomains even if paraview is launched just as a client (no pvservers).

To remove those frontiers we would need to use the "GhostCellGenerator" filter even
if parallelism is not used (paraview used as a client).
"""
# import paraview
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *

from pathlib import Path
from common import THEMYSSERVERPLUGINS_TESTING_PATH

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XML MultiBlock Data Reader'
a4mats_vortex_8procsvtm = XMLMultiBlockDataReader(
    registrationName="4mats_vortex_8procs.vtm",
    FileName=[
        f"{THEMYSSERVERPLUGINS_TESTING_PATH}/ContourWriter/4MatsVortex_8procs/4mats_vortex_8procs.vtm"
    ],
)


# get active view
renderView1 = GetActiveViewOrCreate("RenderView")

# show data in view
a4mats_vortex_8procsvtmDisplay = Show(
    a4mats_vortex_8procsvtm, renderView1, "GeometryRepresentation"
)

# trace defaults for the display properties.
a4mats_vortex_8procsvtmDisplay.Representation = "Surface"

# changing interaction mode based on data extents
renderView1.InteractionMode = "2D"
renderView1.CameraPosition = [0.5, 0.5, 3.35]
renderView1.CameraFocalPoint = [0.5, 0.5, 0.0]


# create a new 'EducationalFilter'
educationalFilter1 = EducationalFilter(
    registrationName="EducationalFilter1",
    Input=a4mats_vortex_8procsvtm,
    FileName="my_file",
)

# Properties modified on educationalFilter1
educationalFilter1.BlockIndices = [2, 7, 12, 17, 22]
educationalFilter1.FileName = "contours_vortex_educational"
educationalFilter1.WritingMode = "Use CEAWriterDat"

# show data in view
educationalFilter1Display = Show(
    educationalFilter1, renderView1, "GeometryRepresentation"
)

# trace defaults for the display properties.
educationalFilter1Display.Representation = "Surface"

# update the view to ensure updated data information
renderView1.Update()

# Path toward reference contour file
# CONTOUR_REFERENCE_PATH = Path(f"/home/billae/paraview_manuel/themys/plugins/Plugins/Data/Testing/CEAEducational/contours_vortex_educational.dat")
CONTOUR_REFERENCE_PATH = Path(
    f"{THEMYSSERVERPLUGINS_TESTING_PATH}/CEAEducational/contours_vortex_educational.dat"
)
path_result = "./" + educationalFilter1.FileName + ".dat"

# Compare the obtained contour with the reference
with CONTOUR_REFERENCE_PATH.open() as file_ref:
    with (Path(path_result)).open() as result:
        assert file_ref.readlines() == result.readlines()
