"""
This test open a database, transforms time related field data into point datasets
and applies the CEAAnnotateTimeFilter using those point datasets.
It compares the obtained image to a reference and also test the exact
value of the annotated text
"""

# import paraview
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 13

#### import the simple module from the paraview
from paraview import servermanager as sm
from paraview.vtk.numpy_interface import dataset_adapter as dsa
from paraview.simple import *

from common import THEMYSSERVERPLUGINS_TESTING_PATH, launch_comparison

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XML PolyData Reader'
SRC_DATA = XMLPolyDataReader(
    registrationName="Vortex_part",
    FileName=[
        f"{THEMYSSERVERPLUGINS_TESTING_PATH}/MaterialFilters/4MatsVortex/HDep-n=Temps_u=s.p-0001_000005/HDep-n=Temps_u=s.p-0001_000005_1_0.vtp"
    ],
)

# Properties modified on hDepnTemps_usp0001_000005_1_0vtp
SRC_DATA.TimeArray = "None"

# get active view
RENDER_VIEW = GetActiveViewOrCreate("RenderView")

# show data in view
SRC_DATA_DISPLAY = Show(SRC_DATA, RENDER_VIEW, "GeometryRepresentation")

# trace defaults for the display properties.
SRC_DATA_DISPLAY.Representation = "Surface"

# reset view to fit data
RENDER_VIEW.ResetCamera(False, 0.9)

# changing interaction mode based on data extents
RENDER_VIEW.InteractionMode = "2D"
RENDER_VIEW.CameraPosition = [0.6500000059604645, 0.5, 3.35]
RENDER_VIEW.CameraFocalPoint = [0.6500000059604645, 0.5, 0.0]

# update the view to ensure updated data information
RENDER_VIEW.Update()

# set scalar coloring
ColorBy(SRC_DATA_DISPLAY, ("CELLS", "Milieu:Density"))

# rescale color and/or opacity maps used to include current data range
SRC_DATA_DISPLAY.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
SRC_DATA_DISPLAY.SetScalarBarVisibility(RENDER_VIEW, True)

# create a new 'Field Data to Attribute'
FIELD_TO_POINTDATA = FieldDatatoAttribute(
    registrationName="FieldDatatoAttribute1", Input=SRC_DATA
)

# Properties modified on fieldDatatoAttribute1
FIELD_TO_POINTDATA.FieldDataArrays = [
    "vtkFixedTimeStep",
    "vtkFixedTimeValue",
    "vtkMaterialId",
]
FIELD_TO_POINTDATA.OutputFieldType = "Point Data"

# show data in view
FIELD_TO_POINTDATA_DISPLAY = Show(
    FIELD_TO_POINTDATA, RENDER_VIEW, "GeometryRepresentation"
)

# trace defaults for the display properties.
FIELD_TO_POINTDATA_DISPLAY.Representation = "Surface"

# hide data in view
Hide(SRC_DATA, RENDER_VIEW)

# show color bar/color legend
FIELD_TO_POINTDATA_DISPLAY.SetScalarBarVisibility(RENDER_VIEW, True)

# update the view to ensure updated data information
RENDER_VIEW.Update()

# create a new 'Annotate Time CEA'
ANNOTATED = AnnotateTimeCEA(
    registrationName="CEAAnnotateTime1", Input=FIELD_TO_POINTDATA
)

# Properties modified on ANNOTATED
ANNOTATED.ArrayAssociation = "Point Data"
ANNOTATED.Expression = (
    "'Temps %f $\\mu$s (#%d)' % (vtkFixedTimeValue[0]*1e6, vtkFixedTimeStep[0])"
)

# show data in view
ANNOTATED_DISPLAY = Show(ANNOTATED, RENDER_VIEW, "TextSourceRepresentation")

# update the view to ensure updated data information
RENDER_VIEW.Update()

# ================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
# ================================================================

# get layout
layout1 = GetLayout()

# --------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
layout1.SetSize(800, 600)

# -----------------------------------
# saving camera placements for views

# current camera placement for renderView1
RENDER_VIEW.InteractionMode = "2D"
RENDER_VIEW.CameraPosition = [0.6500000059604645, 0.5, 2.358125450647677]
RENDER_VIEW.CameraFocalPoint = [0.6500000059604645, 0.5, 0.0]
RENDER_VIEW.CameraParallelScale = 0.6103277773685832

launch_comparison(RENDER_VIEW, baseline_relative_dir="PluginsPython")

database = dsa.WrapDataObject(sm.Fetch(ANNOTATED))
assert (
    database.GetRowData().GetArray(0).GetValue(0) == "Temps 1000280.412347 $\\mu$s (#5)"
)
