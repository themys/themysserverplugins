"""
This module tests that CEASmoothIsoVolumeFilter is functional.

It loads a 3D vtkUnstructuredGrid object made of 2 mats and runs CEASmoothIsoVolumeFilter.
"""
# import paraview
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 13

#### import the simple module from the paraview
from paraview.simple import *

from common import THEMYSSERVERPLUGINS_TESTING_PATH, launch_comparison


#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XML MultiBlock Data Reader'
frac_vol_2vtm = XMLMultiBlockDataReader(
    registrationName="test_frac_vol_2.vtm",
    FileName=[
        f"{THEMYSSERVERPLUGINS_TESTING_PATH}/CEASmoothIsoVolume/frac_vol_2/test_frac_vol_2.vtm"
    ],
)

# get active view
render_view = GetActiveViewOrCreate("RenderView")

# show data in view
test_frac_vol_2vtm_display = Show(
    frac_vol_2vtm, render_view, "UnstructuredGridRepresentation"
)

# trace defaults for the display properties.
test_frac_vol_2vtm_display.Representation = "Surface"

smooth_interfaces = CEASmoothInterfacesFilter(
    registrationName="CEASmoothIsoVolumeFilter", Input=frac_vol_2vtm
)

# Properties modified on cEASmoothIsoVolumeFilter1
smooth_interfaces.Maximum = 1.2
smooth_interfaces.WeightCellOption = "Hexahedron"

# show data in view
smooth_interfaces_display = Show(
    smooth_interfaces, render_view, "UnstructuredGridRepresentation"
)

# trace defaults for the display properties.
smooth_interfaces_display.Representation = "Surface"

# hide data in view
Hide(frac_vol_2vtm, render_view)

# update the view to ensure updated data information
render_view.Update()

# create a new 'Clip'
clip_x = Clip(registrationName="ClipX", Input=smooth_interfaces)

# show data in view
clip_x_display = Show(clip_x, render_view, "UnstructuredGridRepresentation")

# trace defaults for the display properties.
clip_x_display.Representation = "Surface"

# hide data in view
Hide(smooth_interfaces, render_view)

# update the view to ensure updated data information
render_view.Update()

# create a new 'Clip'
clip_z = Clip(registrationName="ClipZ", Input=clip_x)

# Properties modified on ClipZ.ClipType
clip_z.ClipType.Normal = [0.0, 0.0, 1.0]

# show data in view
clip_z_display = Show(clip_z, render_view, "UnstructuredGridRepresentation")

# trace defaults for the display properties.
clip_z_display.Representation = "Surface"

# hide data in view
Hide(clip_x, render_view)

# hide colorbar
clip_z_display.SetScalarBarVisibility(render_view, False)

# Properties modified on clip2Display
clip_z_display.BlockSelectors = ["/Root/Mat1"]

# toggle interactive widget visibility (only when running from the GUI)
# HideInteractiveWidgets(proxy=clip_z_display.ClipType)

# update the view to ensure updated data information
render_view.Update()

# ================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
# ================================================================

# get layout
layout1 = GetLayout()

# --------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
layout1.SetSize(800, 600)

# -----------------------------------
# saving camera placements for views

# current camera placement for renderView1
render_view.CameraPosition = [13.00269921654146, 2.0471101104102107, 23.494793916639182]
render_view.CameraFocalPoint = [2.5, 5.0, 2.5]
render_view.CameraViewUp = [
    0.4536834880248553,
    0.8852547798168351,
    -0.10244641284617008,
]
render_view.CameraViewAngle = 33.06451612903226
render_view.CameraParallelScale = 6.123724356957945
# remove orientation axes
render_view.OrientationAxesVisibility = 0

# update the view to ensure updated data information
render_view.Update()

launch_comparison(render_view, baseline_relative_dir="PluginsPython")
