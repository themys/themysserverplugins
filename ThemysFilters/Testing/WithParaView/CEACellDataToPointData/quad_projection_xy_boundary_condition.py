import paraview

paraview.compatibility.major = 5
paraview.compatibility.minor = 13

#### import the simple module from the paraview
from paraview.simple import *

from common import CMAKE_SOURCE_DIR, launch_comparison

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XML MultiBlock Data Reader'
hDepnTemps_usp0001_000005vtm = XMLMultiBlockDataReader(registrationName='HDep-n=Temps_u=s.p-0001_000005.vtm', FileName=[f'{CMAKE_SOURCE_DIR}/Data/Testing/MaterialFilters/4MatsVortex/HDep-n=Temps_u=s.p-0001_000005.vtm'])

# get animation scene
animationScene1 = GetAnimationScene()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# show data in view
hDepnTemps_usp0001_000005vtmDisplay = Show(hDepnTemps_usp0001_000005vtm, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
hDepnTemps_usp0001_000005vtmDisplay.Representation = 'Surface'

# reset view to fit data
renderView1.ResetCamera(False, 0.9)

#changing interaction mode based on data extents
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.5, 0.5, 3.35]
renderView1.CameraFocalPoint = [0.5, 0.5, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(hDepnTemps_usp0001_000005vtmDisplay, ('FIELD', 'vtkBlockColors'))

# show color bar/color legend
hDepnTemps_usp0001_000005vtmDisplay.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'vtkBlockColors'
vtkBlockColorsLUT = GetColorTransferFunction('vtkBlockColors')

# get opacity transfer function/opacity map for 'vtkBlockColors'
vtkBlockColorsPWF = GetOpacityTransferFunction('vtkBlockColors')

# get 2D transfer function for 'vtkBlockColors'
vtkBlockColorsTF2D = GetTransferFunction2D('vtkBlockColors')

# set scalar coloring
ColorBy(hDepnTemps_usp0001_000005vtmDisplay, ('CELLS', 'vtkInterfaceFraction'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(vtkBlockColorsLUT, renderView1)

# rescale color and/or opacity maps used to include current data range
hDepnTemps_usp0001_000005vtmDisplay.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
hDepnTemps_usp0001_000005vtmDisplay.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'vtkInterfaceFraction'
vtkInterfaceFractionLUT = GetColorTransferFunction('vtkInterfaceFraction')

# get opacity transfer function/opacity map for 'vtkInterfaceFraction'
vtkInterfaceFractionPWF = GetOpacityTransferFunction('vtkInterfaceFraction')

# get 2D transfer function for 'vtkInterfaceFraction'
vtkInterfaceFractionTF2D = GetTransferFunction2D('vtkInterfaceFraction')

# create a new 'CEA Cell Data to Point Data'
cEACellDatatoPointData1 = CEACellDatatoPointData(registrationName='CEACellDatatoPointData1', Input=hDepnTemps_usp0001_000005vtm)

# Properties modified on cEACellDatatoPointData1
cEACellDatatoPointData1.ProcessAllArrays = 0
cEACellDatatoPointData1.CellDataArraytoprocess = ['vtkInterfaceFraction']
cEACellDatatoPointData1.WeightCellOption = 'Quad'
cEACellDatatoPointData1.BoundaryConditionPoint = 'X Axis'
cEACellDatatoPointData1.BoundaryConditionPoint2 = 'Y Axis'

# show data in view
cEACellDatatoPointData1Display = Show(cEACellDatatoPointData1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
cEACellDatatoPointData1Display.Representation = 'Surface'

# hide data in view
Hide(hDepnTemps_usp0001_000005vtm, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(cEACellDatatoPointData1Display, ('FIELD', 'vtkBlockColors'))

# show color bar/color legend
cEACellDatatoPointData1Display.SetScalarBarVisibility(renderView1, True)

# set scalar coloring
ColorBy(cEACellDatatoPointData1Display, ('POINTS', 'vtkInterfaceFraction'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(vtkBlockColorsLUT, renderView1)

# rescale color and/or opacity maps used to include current data range
cEACellDatatoPointData1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
cEACellDatatoPointData1Display.SetScalarBarVisibility(renderView1, True)

# create a new 'Iso Volume'
isoVolume1 = IsoVolume(registrationName='IsoVolume1', Input=cEACellDatatoPointData1)

# Properties modified on isoVolume1
isoVolume1.ThresholdRange = [0.5, 2.0]

# show data in view
isoVolume1Display = Show(isoVolume1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
isoVolume1Display.Representation = 'Surface'

# hide data in view
Hide(cEACellDatatoPointData1, renderView1)

# show color bar/color legend
isoVolume1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# hide color bar/color legend
isoVolume1Display.SetScalarBarVisibility(renderView1, False)

# Hide orientation axes
renderView1.OrientationAxesVisibility = 0

# reset view to fit data
renderView1.ResetCamera(True, 0.9)

#================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
#================================================================

# get layout
layout1 = GetLayout()

#--------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
layout1.SetSize(800, 600)

#-----------------------------------
# saving camera placements for views

# current camera placement for renderView1
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.5, 0.5, 2.7320508075688776]
renderView1.CameraFocalPoint = [0.5, 0.5, 0.0]
renderView1.CameraParallelScale = 0.5550569790535544

launch_comparison(renderView1, baseline_relative_dir="CEACellDataToPointData")