#include "vtkHDFFieldDataReader.h"

#include <algorithm> // for fill
#include <array>     // for array
#include <cassert>
#include <cstddef> // for size_t
#include <limits>
#include <sstream>
#include <string>
#include <vector> // for vector

#include <H5Ipublic.h>        // for hid_t
#include <H5Ppublic.h>        // for H5P_DEFAULT
#include <H5version.h>        // for H5Gopen
#include <vtkAbstractArray.h> // for vtkAbstractArray
#include <vtkDataObject.h>    // for vtkDataObject
#include <vtkHDFUtilities.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkObjectFactory.h> // for vtkStandardNewMacro
#include <vtkSetGet.h>        // for vtkErrorMacro
#include <vtkSmartPointer.h>
#include <vtkSmartPointerBase.h> // for operator==, vtkSmartPointerBase
#include <vtkStdString.h>        // for operator<<
#include <vtkStringArray.h>
#include <vtkTable.h>
#include <vtkType.h>    // for vtkIdType, VTK_MULTIBLOCK_DATA_SET
#include <vtkVariant.h> // for vtkVariant

//----------------------------------------------------------------------------
struct vtkHDFFieldDataReader::vtkInternals {
  vtkInternals()
  {
    std::fill(this->AttributeDataGroup.begin(), this->AttributeDataGroup.end(),
              -1);
    std::fill(this->Version.begin(), this->Version.end(), 0);
  }

  //------------------------------------------------------------------------------
  bool Open(const std::string& filename)
  {
    if (!vtkHDFUtilities::Open(filename.c_str(), this->File))
    {
      return false;
    }
    return this->RetrieveHDFInformation(vtkHDFUtilities::VTKHDF_ROOT_PATH);
  }

  //------------------------------------------------------------------------------
  bool OpenGroupAsVTKGroup(const std::string& groupPath)
  {
    this->VTKGroup = H5Gopen(this->File, groupPath.c_str(), H5P_DEFAULT);
    // VTKGroup strictly negative means the file doesn't exist or we try to read
    // a non-VTKHDF file
    return this->VTKGroup >= 0;
  }

  //------------------------------------------------------------------------------
  bool RetrieveHDFInformation(const std::string& rootName)
  {
    return vtkHDFUtilities::RetrieveHDFInformation(
        this->File, this->VTKGroup, rootName, this->Version, this->DataSetType,
        this->NumberOfPieces, this->AttributeDataGroup);
  }

  hid_t File{0};
  hid_t VTKGroup{0};
  std::array<hid_t, 3> AttributeDataGroup{};
  int DataSetType{0};
  int NumberOfPieces{0};
  std::array<int, 2> Version{};
};

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkHDFFieldDataReader);

//----------------------------------------------------------------------------
vtkHDFFieldDataReader::vtkHDFFieldDataReader()
    : Internals(new vtkHDFFieldDataReader::vtkInternals())
{
  this->SetNumberOfInputPorts(0);
  this->SetNumberOfOutputPorts(1);
}

//----------------------------------------------------------------------------
vtkHDFFieldDataReader::~vtkHDFFieldDataReader() = default;

//------------------------------------------------------------------------------
int vtkHDFFieldDataReader::RequestInformation(
    vtkInformation* /*request*/, vtkInformationVector** /*inputVector*/,
    vtkInformationVector* outputVector)
{
  if (this->GetFileName().empty())
  {
    vtkErrorMacro("Requires valid input file name");
    return 0;
  }

  if (!this->Internals->Open(this->GetFileName()))
  {
    vtkErrorMacro("Could not open file " << this->GetFileName());
    return 0;
  }
  const vtkInformation* outInfo = outputVector->GetInformationObject(0);
  if (outInfo == nullptr)
  {
    vtkErrorMacro("Invalid output information object");
    return 0;
  }
  return 1;
}

//------------------------------------------------------------------------------
int vtkHDFFieldDataReader::RequestData(vtkInformation* /*request*/,
                                       vtkInformationVector** /*inputVector*/,
                                       vtkInformationVector* outputVector)
{
  vtkInformation* outInfo = outputVector->GetInformationObject(0);
  if (outInfo == nullptr)
  {
    return 0;
  }
  vtkDataObject* output = outInfo->Get(vtkDataObject::DATA_OBJECT());
  if (output == nullptr)
  {
    return 0;
  }
  vtkTable* table = vtkTable::SafeDownCast(output);
  if (table == nullptr)
  {
    return 0;
  }

  auto& internals = (*this->Internals);
  if (internals.DataSetType == VTK_MULTIBLOCK_DATA_SET ||
      internals.DataSetType == VTK_PARTITIONED_DATA_SET_COLLECTION)
  {
    const std::vector<std::string> datasets =
        vtkHDFUtilities::GetOrderedChildrenOfGroup(
            internals.VTKGroup, vtkHDFUtilities::VTKHDF_ROOT_PATH);

    const auto& sentinel = std::numeric_limits<std::size_t>::max();
    std::size_t size{sentinel};

    for (const auto& datasetName : datasets)
    {
      if (datasetName == "Assembly")
      {
        continue;
      }
      const std::string hdfPathName = [&datasetName]() {
        std::ostringstream buffer;
        buffer << vtkHDFUtilities::VTKHDF_ROOT_PATH << "/" << datasetName;
        return buffer.str();
      }();
      internals.RetrieveHDFInformation(hdfPathName);
      internals.OpenGroupAsVTKGroup(hdfPathName); // Change root

      if (size == sentinel)
      {
        size = vtkHDFUtilities::GetNumberOfSteps(internals.VTKGroup);
      } else if (size != vtkHDFUtilities::GetNumberOfSteps(internals.VTKGroup))
      {
        vtkErrorMacro("An inconsistent number of timesteps between blocks is "
                      "not supported.");
        return 0;
      }

      this->ReadFieldData(table, datasetName.c_str());
    }
  } else
  {
    this->ReadFieldData(table);
  }
  return 1;
}

//------------------------------------------------------------------------------
void vtkHDFFieldDataReader::ReadFieldData(vtkTable* output,
                                          const char* namePrefix)
{
  auto& internals = (*this->Internals);
  const std::size_t nStep =
      vtkHDFUtilities::GetNumberOfSteps(internals.VTKGroup);
  const bool hasTemporalData = nStep > 1;
  const std::vector<std::string> names = vtkHDFUtilities::GetArrayNames(
      internals.AttributeDataGroup, vtkDataObject::FIELD);

  for (const std::string& name : names)
  {
    auto destArray = vtkSmartPointer<vtkStringArray>::New();
    destArray->SetNumberOfComponents(1);
    assert(nStep < std::numeric_limits<vtkIdType>::max());
    destArray->SetNumberOfTuples(static_cast<vtkIdType>(nStep));
    for (vtkIdType step = 0; step < nStep; ++step)
    {
      vtkIdType offset = -1;
      std::array<vtkIdType, 2> size = {-1, -1};
      if (hasTemporalData)
      {
        size =
            vtkHDFUtilities::GetFieldArraySize(internals.VTKGroup, step, name);
        offset = vtkHDFUtilities::GetArrayOffset(internals.VTKGroup, step,
                                                 vtkDataObject::FIELD, name);
      }
      if (size[0] == 0 && size[1] == 0)
      {
        continue;
      }
      const auto array = vtk::TakeSmartPointer(vtkHDFUtilities::NewFieldArray(
          internals.AttributeDataGroup, name.c_str(), offset, size[1],
          size[0]));
      if (array == nullptr)
      {
        vtkErrorMacro("Error reading array " << name);
        return;
      }
      std::stringstream buffer;
      for (vtkIdType valueIdx = 0; valueIdx < array->GetNumberOfValues();
           ++valueIdx)
      {
        buffer << array->GetVariantValue(valueIdx).ToString();
        if (valueIdx < array->GetNumberOfValues() - 1)
        {
          buffer << ";";
        }
      }
      destArray->InsertValue(step, buffer.str());
    }

    std::string arrayName(namePrefix);
    if (!arrayName.empty())
    {
      arrayName += "_";
    }
    arrayName += name;

    destArray->SetName(arrayName.c_str());
    output->AddColumn(destArray);
  }
}
