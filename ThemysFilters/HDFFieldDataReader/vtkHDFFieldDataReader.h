#ifndef vtkHDFFieldDataReader_h
#define vtkHDFFieldDataReader_h

#include <memory>
#include <string> // for basic_string, string

#include <vtkObject.h> // for vtkObject
#include <vtkSetGet.h>
#include <vtkTableAlgorithm.h>

class vtkInformation;
class vtkInformationVector;
class vtkTable;

/**
 * This vtkHDFFieldDataReader reads field data from vtkHDF files for all
 * timesteps in a vtkTable. Each row represents a timestep and each column
 * represents a field data array.
 */
class vtkHDFFieldDataReader : public vtkTableAlgorithm
{
public:
  static vtkHDFFieldDataReader* New();
  vtkTypeMacro(vtkHDFFieldDataReader, vtkTableAlgorithm);

  ///@{
  /**
   * Get/Set the name of the input file.
   */
  vtkSetMacro(FileName, std::string);
  vtkGetMacro(FileName, std::string);
  ///@}

protected:
  vtkHDFFieldDataReader();
  ~vtkHDFFieldDataReader() override;

  ///@{
  /**
   * Standard functions to specify the information and read the data from the
   * file.
   */
  int RequestInformation(vtkInformation* request,
                         vtkInformationVector** inputVector,
                         vtkInformationVector* outputVector) override;
  int RequestData(vtkInformation* request, vtkInformationVector** inputVector,
                  vtkInformationVector* outputVector) override;
  ///@}

private:
  vtkHDFFieldDataReader(const vtkHDFFieldDataReader&) = delete;
  void operator=(const vtkHDFFieldDataReader&) = delete;

  /**
   * Read all steps of field data from the file and add them to the table,
   * concatenating all tuples and components to have keep only one step by row.
   */
  void ReadFieldData(vtkTable* output, const char* namePrefix = "");

  std::string FileName;

  struct vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif
