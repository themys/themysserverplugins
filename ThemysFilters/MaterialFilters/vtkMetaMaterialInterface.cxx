#include "vtkMetaMaterialInterface.h"

#include <ostream> // for basic_ostream, char_traits

#include <vtkGarbageCollector.h>
#include <vtkIndent.h> // for operator<<, vtkIndent
#include <vtkObjectFactory.h>
#include <vtkSmartPointerBase.h>        // for operator!=, operator==, vtkS...
#include <vtkYoungsMaterialInterface.h> // for vtkYoungsMaterialInterface

#include "vtkMaterialInterface.h" // for vtkMaterialInterface

class vtkInformation;
class vtkInformationVector;
//--------------------------------------------------------------------------------------------------
vtkStandardNewMacro(vtkMetaMaterialInterface);

//--------------------------------------------------------------------------------------------------
vtkMetaMaterialInterface::vtkMetaMaterialInterface() = default;

// --------------------------------------------------------------------------------------------------
vtkMetaMaterialInterface::~vtkMetaMaterialInterface() = default;

// --------------------------------------------------------------------------------------------------
int vtkMetaMaterialInterface::RequestData(vtkInformation* request,
                                          vtkInformationVector** inputVector,
                                          vtkInformationVector* outputVector)
{
  if (this->UseYoungs)
  {
    if (this->YoungsMaterialInterface == nullptr)
    {
      vtkErrorMacro("No YoungsMaterialInterface defined");
      return 0;
    }
    return this->YoungsMaterialInterface->ProcessRequest(request, inputVector,
                                                         outputVector);
  }

  if (this->MaterialInterface == nullptr)
  {
    vtkErrorMacro("No MaterialInterface defined");
    return 0;
  }
  return this->MaterialInterface->ProcessRequest(request, inputVector,
                                                 outputVector);
}
//--------------------------------------------------------------------------------------------------
void vtkMetaMaterialInterface::PrintSelf(ostream& ostrm, vtkIndent indent)
{
  this->Superclass::PrintSelf(ostrm, indent);
  ostrm << indent << "UseYoungs: " << this->UseYoungs << "\n";

  if (this->YoungsMaterialInterface != nullptr)
  {
    ostrm << indent << "YoungsMaterialInterface:" << endl;
    this->YoungsMaterialInterface->PrintSelf(ostrm, indent.GetNextIndent());
  } else
  {
    ostrm << indent << "YoungsMaterialInterface: (null)" << endl;
  }

  if (this->MaterialInterface != nullptr)
  {
    ostrm << indent << "MaterialInterface:" << endl;
    this->MaterialInterface->PrintSelf(ostrm, indent.GetNextIndent());
  } else
  {
    ostrm << indent << "MaterialInterface: (null)" << endl;
  }
}

//--------------------------------------------------------------------------------------------------
void vtkMetaMaterialInterface::ReportReferences(vtkGarbageCollector* collector)
{
  this->Superclass::ReportReferences(collector);
  vtkGarbageCollectorReport(collector, this->YoungsMaterialInterface,
                            "MetaYoungsMaterialInterface");
  vtkGarbageCollectorReport(collector, this->MaterialInterface,
                            "MetaMaterialInterface");
}
