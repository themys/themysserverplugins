/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include "vtkMaterialInterface_Utils.h"

#include <array>
#include <cmath>
#include <string> // for char_traits
#include <string_view>
#include <vector>

#include <vtkCleanPolyData.h>
#include <vtkDoubleArray.h>
#include <vtkGenericDataArray.txx> // for vtkGenericDataArr...
#include <vtkNew.h>
#include <vtkSmartPointer.h>
#include <vtkType.h>

#include "vtkMaterialInterface_InterfaceNormale.h"
#include "vtkMaterialInterface_Types.h"

/*----------------------------------------------------------------------------*/
/**
 * @brief Return true if the name is empty or equal to None
 *
 * @param name : the name to check
 * @return true
 * @return false
 */
/*----------------------------------------------------------------------------*/
bool is_empty_name(std::string_view name)
{
  return name == "None" || name.empty();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkSmartPointer<vtkDoubleArray>
compute_distance_to_plan(const std::vector<Vec3>& pts_coords,
                         const InterfaceNormale& _normal,
                         const double _distance, const bool _opposite)
{
  auto distance = vtkSmartPointer<vtkDoubleArray>::New();
  const char sign = _opposite ? -1 : 1;
  const auto pts_coords_len{pts_coords.size()};
  distance->SetNumberOfTuples(static_cast<vtkIdType>(pts_coords_len));
  for (vtkIdType iPt = 0; iPt < pts_coords.size(); ++iPt)
  {
    const auto& coords = pts_coords[iPt];
    const double value =
        sign * (_normal.at(0) * coords.at(0) + _normal.at(1) * coords.at(1) +
                _normal.at(2) * coords.at(2) + _distance);
    distance->SetTuple1(iPt, value);
  }
  return distance;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkIdType size_estimation(const vtkIdType nb_items)
{
  constexpr vtkIdType power_of_two{1024};
  if (nb_items > power_of_two)
  {
    return static_cast<vtkIdType>(std::floor(nb_items / power_of_two)) *
           power_of_two;
  }
  return power_of_two;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkSmartPointer<vtkPolyData>
applyCleanPolyData(const vtkSmartPointer<vtkPolyData>& input)
{
  constexpr const int TRUE = 1;
  constexpr const int FALSE = 0;
  auto cleanFilter = vtkNew<vtkCleanPolyData>();
  cleanFilter->SetInputData(input);
  cleanFilter->SetPieceInvariant(TRUE);
  cleanFilter->SetTolerance(0.0);
  cleanFilter->SetAbsoluteTolerance(1.0);
  cleanFilter->SetToleranceIsAbsolute(FALSE);
  cleanFilter->SetConvertLinesToPoints(TRUE);
  cleanFilter->SetConvertPolysToLines(TRUE);
  cleanFilter->SetConvertStripsToPolys(TRUE);
  cleanFilter->SetPointMerging(TRUE);
  cleanFilter->Update();
  return cleanFilter->GetOutput();
}
