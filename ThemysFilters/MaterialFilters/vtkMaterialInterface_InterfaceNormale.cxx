/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include "vtkMaterialInterface_InterfaceNormale.h"

#include <ostream>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
InterfaceNormale::operator bool() const
{
  return this->at(0) != 0. || this->at(1) != 0. || this->at(2) != 0.;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, const InterfaceNormale& ifn)
{
  out << "(" << ifn.at(0) << ", " << ifn.at(1) << ", " << ifn.at(2) << ")";
  return out;
}
